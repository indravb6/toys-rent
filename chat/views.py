from django.shortcuts import render, redirect
from django.db import connection
from random import randint
from hashlib import md5
from datetime import datetime

from user.helpers import login_required
from toysrent.helpers import dictfetchall


def render_chat(request, no_ktp, is_admin):
    cursor = connection.cursor()

    selector = "no_ktp_admin" if is_admin else "no_ktp_anggota"
    target_ktp = "anggota_no_ktp" if is_admin else "admin_no_ktp"
    target_nama = "anggota_nama" if is_admin else "admin_nama"

    cursor.execute(
        """SELECT
            b.nama_lengkap AS anggota_nama,
            b.no_ktp AS anggota_no_ktp,
            c.nama_lengkap AS admin_nama,
            c.no_ktp AS admin_no_ktp,
            pesan,
            date_time
            FROM chat a
        JOIN PENGGUNA b ON b.no_ktp = a.no_ktp_anggota
        JOIN PENGGUNA c ON c.no_ktp = a.no_ktp_admin
        WHERE """ + selector + """ = %s""",
        [no_ktp])
    rows = dictfetchall(cursor)

    cursor.execute(
        "SELECT nama_lengkap, no_ktp FROM PENGGUNA NATURAL JOIN ADMIN")
    rows2 = dictfetchall(cursor)
    cursor.close()

    users = set([(row.get(target_ktp), row.get(target_nama))
                 for row in rows])

    if not is_admin:
        users.update([(row.get("no_ktp"), row.get("nama_lengkap"))
                      for row in rows2])

    chat_data = {no_ktp: (nama, [])
                 for no_ktp, nama in users}
    for row in rows:
        user = row.get(target_ktp)
        chat = chat_data.get(user)[1]
        date_time = row.get('date_time').__str__()
        # karena kesalahan pada struktur database (dari soal)
        is_from_admin = randint(0, 1) == 0
        chat_from = row.get(
            'admin_nama') if is_from_admin else row.get('anggota_nama')
        message = row.get('pesan')
        chat.append({"from": chat_from, "message": message,
                     "date_time": date_time, "is_from_admin": is_from_admin})

    return render(request, 'chat.html', {"chat": chat_data})


@login_required
def chat(request, no_ktp, is_admin):
    if request.method == "POST":
        chat_from = no_ktp
        chat_to = request.POST["to"]
        chat_message = request.POST["message"]
        chat_date_time = datetime.now().strftime("%Y/%m/%d, %H:%M:%S")
        id = md5(chat_date_time.encode()).hexdigest()[:15]

        cursor = connection.cursor()
        cursor.execute(
            """INSERT INTO chat (id, no_ktp_anggota, no_ktp_admin, pesan, date_time) 
            VALUES (%s, %s, %s, %s, %s)""",
            [id, chat_to if is_admin else chat_from, chat_from if is_admin else chat_to, chat_message, chat_date_time])
        cursor.close()

        return redirect("/chat/")

    return render_chat(request, no_ktp, is_admin)
