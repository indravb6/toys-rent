from django.shortcuts import render, redirect, HttpResponse
from django.db import connection

from math import ceil

from user.helpers import login_required
from toysrent.helpers import dictfetchall

# Create your views here.


@login_required
def review_barang(request, no_ktp, is_admin):
    page = int(request.GET.get('page', 1))

    cursor = connection.cursor()

    for_admin = ", pg.nama_lengkap " if is_admin else " "
    for_member = " " if is_admin else " AND pg.no_ktp = " + no_ktp + "::varchar "

    cursor.execute(
        """SELECT CAST(p.id_pemesanan AS INT) AS id_pemesanan,
            b.nama_item AS nama_item,
            bk.review AS review,
            bk.tanggal_review AS tanggal_review,
            bk.no_resi AS no_resi,
            bk.no_urut AS no_urut"""
        + for_admin +
        """FROM pengiriman p, barang b, barang_dikirim bk, pengguna pg
        WHERE bk.no_resi = p.no_resi AND
            b.id_barang = bk.id_barang AND
            p.no_ktp_anggota = pg.no_ktp AND
            bk.review <> ''"""
        + for_member +
        """ORDER BY id_pemesanan DESC"""
    )

    data = dictfetchall(cursor)
    cursor.close()

    print(data)

    jml_page = ceil(len(data) / 10)
    is_empty = jml_page == 0

    review = data[(page-1)*10:page*10]

    page_data = {
        "review": review,
        "is_admin": is_admin,
        "is_empty": is_empty,
        "jml_page": list(range(1, jml_page+1)),
        "page": page
    }

    return render(request, "review-barang.html", page_data)


@login_required
def create_review_barang(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if is_admin:
        return redirect("/review-barang/")

    if request.method == 'POST':
        no_resi = request.POST.get('resi')
        no_urut = request.POST.get('barang-pesanan')
        review = request.POST.get('review')

        cursor.execute(
            "update barang_dikirim set review = '" + review + "'" +
            "where no_resi = " + no_resi + "::varchar " +
            "and no_urut = " + no_urut + "::varchar"
        )

        return redirect('/review-barang/')

    no_resi = request.GET.get('resi', 0)

    if no_resi == 0:
        return redirect('/review-barang/')

    cursor.execute(
        "select no_urut, nama_item from barang_dikirim, barang" +
        " where barang_dikirim.id_barang = barang.id_barang AND no_resi = " +
        no_resi + "::varchar"
    )

    data = dictfetchall(cursor)

    return render(request, "create-review-barang.html", {"data": data, "resi": no_resi})


@login_required
def update_review_barang(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if request.method == 'POST':
        no_resi = request.POST.get('resi')
        no_urut = request.POST.get('urut')
        review = request.POST.get('review')

        cursor.execute(
            "update barang_dikirim set review = '" + review + "' " +
            "where no_resi = " + no_resi + "::varchar " +
            "and no_urut = " + no_urut + "::varchar"
        )

        return redirect("/review-barang/")

    if is_admin:
        return redirect("/review-barang/")

    no_resi = request.GET.get('resi', 0)
    no_urut = request.GET.get('urut', 0)

    if no_resi == 0 or no_urut == 0:
        return redirect("/review-barang/")

    cursor.execute(
        """SELECT no_resi, no_urut, review, nama_item
        FROM barang_dikirim, barang
        WHERE barang.id_barang = barang_dikirim.id_barang AND
        no_resi = """ + no_resi + "::varchar" +
        """ AND no_urut = """ + no_urut + "::varchar"
    )

    data = dictfetchall(cursor)[0]
    cursor.close()

    return render(request, "update-review-barang.html", data)


@login_required
def delete_review_barang(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if is_admin:
        return redirect("/review-barang/")

    if request.method == "POST":
        no_resi = request.POST.get('no-resi', 0)
        no_urut = request.POST.get('no-urut', 0)

        if no_resi == 0 or no_urut == 0:
            return redirect("/review-barang/")

        cursor.execute(
            "update barang_dikirim set review = ''" +
            "where no_resi = " + no_resi + "::varchar " +
            "and no_urut = " + no_urut + "::varchar"
        )

    cursor.close()
    return redirect("/review-barang/")
