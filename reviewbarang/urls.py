from django.urls import path

from .views import review_barang, create_review_barang, update_review_barang, delete_review_barang

urlpatterns = [
    path('', review_barang, name="review-barang"),
    path('create/', create_review_barang, name="create-review-barang"),
    path('update/', update_review_barang, name="update-review-barang"),
    path('delete/', delete_review_barang, name="delete-review-barang"),
]
