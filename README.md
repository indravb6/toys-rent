# Toys Rent

TK4 BASDAT, Group B12

http://toys-rent-b12.herokuapp.com/

Setup local DB:

1. create database:
   - `$ psql`
   - `postgres=> create database toys_rent;`
   - `postgres=>\q`

2. create schema:
   - `$ psql -d toys_rent`
   - `postgres=> create schema toys_rent;`
   - `postgres=> \q`

3. import data:
   - `$ psql -d toys_rent < db2018026.sql`

4. set your environment variables:
   - DATABASE_USER
   - DATABASE_PASSWORD

Anggota:

- Muhammad Indra Ramadhan (1706028695)
- Gagah Pangeran Rosfatiputra (1706039566)
- Reynaldo Wijaya Hendry (1706028625)
- Ferro Geraldi Hardian (1706028612)