from django.urls import path

from .views import create_pengiriman, pengiriman, update_pengiriman, delete_pengiriman

urlpatterns = [
    path('', pengiriman, name="list-pengiriman"),
    path('create/', create_pengiriman, name="create-pengiriman"),
    path('update/', update_pengiriman, name="update-pengiriman"),
    path('delete/', delete_pengiriman, name="delete-pengiriman")
]
