from django.shortcuts import render, HttpResponse, redirect
from django.db import connection

import datetime
from math import ceil

from user.helpers import login_required
from toysrent.helpers import dictfetchall

# Create your views here.


@login_required
def pengiriman(request, no_ktp, is_admin):
    page = int(request.GET.get('page', 1))

    cursor = connection.cursor()

    cursor.execute(
        """SELECT CAST(pgr.no_resi AS INT) AS no_resi,
            pms.id_pemesanan,
            (pgr.ongkos + pms.harga_sewa) as total,
            pgr.tanggal,
            pms.status
        FROM pengiriman pgr, pemesanan pms, pengguna pgn
        WHERE pgr.id_pemesanan = pms.id_pemesanan AND
            pms.no_ktp_pemesan = pgr.no_ktp_anggota AND
            pgr.no_ktp_anggota = pgn.no_ktp AND
            pgn.no_ktp = """ + no_ktp + "::varchar " +
        "ORDER BY no_resi DESC"
    )

    pengiriman = dictfetchall(cursor)

    for pgr in pengiriman:
        resi = str(pgr['no_resi'])
        cursor.execute(
            """select b.nama_item
            from barang_dikirim bk, barang b
            where bk.id_barang = b.id_barang and
            no_resi = """ + resi + "::varchar"
        )

        barang = dictfetchall(cursor)

        pgr['barang'] = barang

    jml_page = ceil(len(pengiriman) / 10)
    is_empty = jml_page == 0

    data = pengiriman[(page-1)*10:page*10]

    page_data = {
        "data": data,
        "is_empty": is_empty,
        "jml_page": list(range(1, jml_page+1)),
        "page": page
    }

    return render(request, "pengiriman.html", page_data)


@login_required
def create_pengiriman(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if request.method == 'POST':
        cursor.execute(
            "SELECT CAST(no_resi AS INT) AS resi FROM pengiriman ORDER BY resi DESC LIMIT 1"
        )

        data = dict(request.POST)

        no_resi = str(dictfetchall(cursor)[0]['resi'] + 1)
        id_pemesanan = data.get('id-pemesanan')[0]
        metode = data.get('metode')[0]
        ongkos = "10000"
        tanggal = data.get('tanggal-pengiriman')[0]
        ktp = str(no_ktp)
        nama_alamat = data.get('alamat-tujuan')[0]

        cursor.execute(
            "INSERT INTO pengiriman VALUES (" +
            no_resi + "," +
            id_pemesanan + ",'" +
            metode + "'," +
            ongkos + ",'" +
            tanggal + "'," +
            ktp + ",'" +
            nama_alamat +
            "')"
        )

        # cursor.execute(
        #     "UPDATE pemesanan SET status = 'Sedang Dikirim' " +
        #     "WHERE id_pemesanan = " + id_pemesanan + "::varchar"
        # )

        urut_id = data.get('urut-id')

        for i in urut_id:
            x = i.split("-")
            no_urut = x[0]
            id_barang = x[1]
            date = str(datetime.datetime.now().date())

            cursor.execute(
                "INSERT INTO barang_dikirim VALUES (" +
                no_resi + "," +
                no_urut + "," +
                id_barang + ",'" +
                date + "',''"
                ")"
            )

            # cursor.execute(
            #     "UPDATE barang_pesanan SET nama_status = 'Sedang Dikirim' " +
            #     "where id_pemesanan = " + id_pemesanan + "::varchar " +
            #     "AND no_urut = " + no_urut + "::varchar"
            # )

        return redirect("/pengiriman/")

    cursor.execute(
        """SELECT id_pemesanan, kuantitas_barang
        FROM pemesanan
        WHERE status = 'Sedang Disiapkan' AND """ +
        "no_ktp_pemesan = " + no_ktp + "::varchar"
    )

    pemesanan = dictfetchall(cursor)

    for pms in pemesanan:
        id = pms['id_pemesanan']
        cursor.execute(
            """SELECT bp.no_urut, b.nama_item, bp.nama_status, b.id_barang
            FROM barang_pesanan bp, barang b
            WHERE bp.id_barang = b.id_barang AND
            id_pemesanan = """ + id + "::varchar"
        )

        pms['barang'] = dictfetchall(cursor)

    cursor.execute(
        "SELECT * FROM alamat WHERE no_ktp_anggota = " + no_ktp + "::varchar"
    )

    alamat = dictfetchall(cursor)

    cursor.close()

    data = {
        "pemesanan": pemesanan,
        "alamat": alamat
    }

    return render(request, "create_pengiriman.html", data)


@login_required
def update_pengiriman(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if request.method == 'POST':
        data = dict(request.POST)
        print(data)

        metode = data['metode'][0]
        nama_alamat = data['alamat-tujuan'][0]
        tanggal = data['tanggal-pengiriman'][0]
        id_pemesanan = data['id-pemesanan'][0]
        no_resi = data['no-resi'][0]
        urut_id = data['urut-id']

        cursor.execute(
            "update pengiriman set metode = " + metode +
            ", tanggal = " + tanggal +
            ", nama_alamat_anggota = " + nama_alamat +
            " where no_resi = " + no_resi
        )

        cursor.execute(
            "select no_urut from barang_dikirim" +
            "where no_resi = " + no_resi
        )

        barang_dikirim = dictfetchall(cursor)

        no_urut = [{i.split("-")[0]: i.split("-")[1]} for i in urut_id]

        date = str(datetime.datetime.now().date())

        for urut in barang_dikirim:
            if urut not in no_urut:
                cursor.execute(
                    "delete from barang_dikirim" +
                    "where no_resi = " + no_resi +
                    " and no_urut = " + urut +
                    date + "',''"
                )

        for urut in no_urut:
            if urut not in barang_dikirim:
                cursor.execute(
                    "insert into barang_dikirim values (" +
                    no_resi + "," +
                    urut + "," +
                    no_urut[urut] + "," +
                    ")"
                )

        return redirect("/pengiriman/")

    id_pemesanan = request.GET.get('id', 0)
    no_resi = request.GET.get('resi', 0)

    if id_pemesanan == 0 or no_resi == 0:
        return redirect("/pengiriman")

    cursor.execute(
        "SELECT metode, tanggal, nama_alamat_anggota, id_pemesanan, no_resi " +
        "FROM pengiriman " +
        "WHERE id_pemesanan = " + id_pemesanan + "::varchar"
        " AND no_resi = " + no_resi + "::varchar"
    )

    pengiriman = dictfetchall(cursor)[0]

    pengiriman['tanggal'] = str(pengiriman['tanggal'])

    cursor.execute(
        """SELECT bp.no_urut, b.nama_item, bp.nama_status, b.id_barang
            FROM barang_pesanan bp, barang b
            WHERE bp.id_barang = b.id_barang AND
            id_pemesanan = """ + id_pemesanan + "::varchar"
    )

    barang = dictfetchall(cursor)

    cursor.execute(
        "SELECT * FROM alamat WHERE no_ktp_anggota = " + no_ktp + "::varchar"
    )

    alamat = dictfetchall(cursor)

    cursor.execute(
        "SELECT id_barang from barang_dikirim where no_resi = " + no_resi + "::varchar"
    )

    barang_dikirim = dictfetchall(cursor)

    for b in barang:
        b['selected'] = False
        for bk in barang_dikirim:
            if b['id_barang'] == bk['id_barang']:
                b['selected'] = True
                break

    for a in alamat:
        if a['nama'] == pengiriman['nama_alamat_anggota']:
            a['selected'] = True
        else:
            a['selected'] = False

    data = {
        "pengiriman": pengiriman,
        "alamat": alamat,
        "barang": barang
    }

    cursor.close()

    return render(request, "update_pengiriman.html", data)


@login_required
def delete_pengiriman(request, no_ktp, is_admin):
    cursor = connection.cursor()
    if request.method == 'POST':
        no_resi = request.POST.get('no-resi')

        cursor.execute(
            "delete from pengiriman where no_resi = " + no_resi + "::varchar"
        )

    cursor.close()
    return redirect("/pengiriman/")
