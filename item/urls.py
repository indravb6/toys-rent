from django.urls import path
from .views import item_list, item_create_update,item_delete

urlpatterns = [
    path('', item_list, name='item-list'),
    path('create/', item_create_update, name='item-create'),
    path('edit/', item_create_update, name='item-edit'),
    path('delete/', item_delete, name='item-delete'),
]
