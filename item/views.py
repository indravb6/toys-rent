from django.db import connection
from django.shortcuts import render, redirect
from django.urls import resolve
from toysrent.helpers import dictfetchall
from user.helpers import login_required, admin_required

from math import ceil

@login_required
def item_list(request, no_ktp, is_admin):
    cursor = connection.cursor()

    page = 1
    if "page" in request.GET.keys():
        page = int(request.GET["page"])

    search_kategori = '%%'
    if "kategori" in request.GET.keys():
        search_kategori = '%' + request.GET["kategori"] + '%'

    order = request.GET.get("order") or "ASC"
        
    if order == 'ASC':
        cursor.execute(
            """SELECT nama, deskripsi, usia_dari, usia_sampai, bahan, nama_kategori as kategori 
            FROM item LEFT JOIN kategori_item ON nama=nama_item
            WHERE nama IN (
                SELECT nama FROM item
                WHERE nama IN (SELECT nama_item FROM kategori_item WHERE LOWER(nama_kategori) LIKE %s) 
                ORDER BY nama LIMIT 10 OFFSET %s)
            ORDER BY nama""", [search_kategori.lower(), 10 * (page - 1)])
    else:
        cursor.execute(
            """SELECT nama, deskripsi, usia_dari, usia_sampai, bahan, nama_kategori as kategori 
            FROM item LEFT JOIN kategori_item ON nama=nama_item
            WHERE nama IN (
                SELECT nama FROM item
                WHERE nama IN (SELECT nama_item FROM kategori_item WHERE LOWER(nama_kategori) LIKE %s) 
                ORDER BY nama DESC LIMIT 10 OFFSET %s)
            ORDER BY nama DESC""", [search_kategori.lower(), 10 * (page - 1)])
    rows = dictfetchall(cursor)
    if search_kategori == '%%':
        cursor.execute(
            """SELECT COUNT(1) as total_item 
            FROM item""", [search_kategori.lower()])
        total_item = dictfetchall(cursor)[0].get("total_item")
    else:
        cursor.execute(
            """SELECT COUNT(1) as total_item 
            FROM item JOIN kategori_item ON nama=nama_item 
            WHERE LOWER(nama_kategori) LIKE %s""", [search_kategori.lower()])
        total_item = dictfetchall(cursor)[0].get("total_item")

    
    items = [row.copy() for row in rows]
    for item in items:
        item['kategori'] = []
    items = [item for i, item in enumerate(items) if item not in items[i+1:]]
    

    for row in rows:
        nama = row.get("nama")
        kategori = row.get("kategori")
        item = next(item for item in items if item.get("nama") == nama)
        item['kategori'].append(kategori)
    
    total_page = ceil(total_item / 10)
    cursor.close()

    data = {"items": items, "pages": range(1, total_page + 1), "order": order,
            "page": page, "is_admin": is_admin, "search_kategori": search_kategori[1:-1]}

    return render(request, 'item.html', data)

@admin_required
def item_create_update(request, no_ktp):
    cursor = connection.cursor()
    cursor.execute("SELECT nama FROM kategori")
    list_kategori = dictfetchall(cursor)
    url_name = resolve(request.path_info).url_name

    item = {}
    edit = False
    if request.method == 'GET':
        nama = request.GET.get("nama")
        if nama:
            cursor.execute("SELECT * FROM item WHERE nama=%s", [nama])
            item = dictfetchall(cursor)[0]
            cursor.execute("SELECT nama_kategori FROM kategori_item WHERE nama_item=%s", [nama])
            item["kategoris"] = [ kategori["nama_kategori" ]for kategori in dictfetchall(cursor)]
            edit = True

    
    data = {
        "list_kategori": list_kategori,
        "item": item,
        "edit": edit
    }

    if request.method == 'POST':
        nama = request.POST["nama"]
        deskripsi = request.POST["deskripsi"]
        usia_dari = request.POST["usia_dari"]
        usia_sampai = request.POST["usia_sampai"]
        bahan = request.POST["bahan"]
        kategoris = request.POST.getlist("nama_kategori")
        
        error = {}
        if not nama:
            error["nama"] = "This field cannot be empty"
        elif url_name == 'item-create':
            cursor.execute("SELECT nama FROM item WHERE nama=%s", [nama])
            if len(dictfetchall(cursor)) > 0:
                error["nama"] = "This name is already in use"
        if not usia_dari or not usia_sampai:
            error["usia"] = "This field cannot be empty"
        elif usia_dari > usia_sampai or int(usia_dari) < 0 or int(usia_sampai) < 0:
            error["usia"] = "Invalid age range"
        if error:
            data["error"] = error
            if url_name == "item-create":
                return render(request, 'item_create.html', data)
            else:
                cursor.execute("SELECT nama FROM item WHERE nama=%s", [nama])
                item = dictfetchall(cursor)[0]
                cursor.execute("SELECT nama_kategori FROM kategori_item WHERE nama_item=%s", [nama])
                item["kategoris"] = [ kategori["nama_kategori" ] for kategori in dictfetchall(cursor)]
                data["item"] = item
                data["edit"] = True
                return render(request, 'item_create.html', data)

        if url_name == 'item-create':
            cursor.execute(
                """INSERT INTO item (nama, deskripsi, usia_dari, usia_sampai, bahan) 
                VALUES (%s, %s, %s, %s, %s)""",
                [nama, deskripsi, usia_dari, usia_sampai, bahan])    
        else:
            cursor.execute(
                """UPDATE item SET deskripsi=%s, usia_dari=%s, usia_sampai=%s, bahan=%s
                WHERE nama=%s""",
                [deskripsi, usia_dari, usia_sampai, bahan, nama])
            cursor.execute("DELETE FROM kategori_item WHERE nama_item=%s", [nama])
        
        for kategori in kategoris:
            cursor.execute(
            """INSERT INTO kategori_item (nama_item, nama_kategori) 
            VALUES (%s, %s)""",
            [nama, kategori])
        
        cursor.close()
        return redirect('/item/')
    cursor.close()
    return render(request, 'item_create.html', data)

@admin_required
def item_delete(request, no_ktp):
    nama = request.POST["nama"]
    print(nama)
    print("haha")
    if request.method == 'POST':
        cursor = connection.cursor()
        cursor.execute("DELETE FROM item WHERE nama = %s", [nama])
        cursor.execute("DELETE FROM kategori_item WHERE nama_item = %s", [nama])

    return redirect("/item/")