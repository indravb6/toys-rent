"""toysrent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import include, path, re_path
from user.views import login

urlpatterns = [
    re_path(r'^', include('pesanan.urls')),
    re_path(r'^', include('level.urls')),
    path('admin/', admin.site.urls),
    path('chat/', include('chat.urls')),
    path('item/', include('item.urls')),
    path('barang/', include('barang.urls')),
    path('pengiriman/', include('pengiriman.urls')),
    path('user/', include('user.urls')),
    path('review-barang/', include('reviewbarang.urls')),
    path('', login, name='login'),
]
