from django.shortcuts import render, redirect
from django.db import connection
from toysrent.helpers import dictfetchall
from user.helpers import login_required


def register(request):
    if request.method == "POST":
        tipe = request.POST["tipe"]
        no_ktp = request.POST["no_ktp"]
        nama_lengkap = request.POST["nama_lengkap"]
        email = request.POST["email"]
        tanggal_lahir = request.POST["tanggal_lahir"]
        no_telp = request.POST["no_telp"]

        nama_alamat = request.POST["nama_alamat"]
        jalan = request.POST["jalan"]
        nomor = request.POST["nomor"]
        kota = request.POST["kota"]
        kodepos = request.POST["kodepos"]

        error = {}
        if not no_ktp:
            error["no_ktp"] = "This field cannot be empty"
        if not email:
            error["email"] = "This field cannot be empty"
        if tipe == "anggota" and not (nama_alamat and jalan and nomor and kota and kodepos):
            error["alamat"] = "All Address fields are required"
        if error:
            return render(request, 'user_register.html', {'error' : error})

        if not tanggal_lahir:
            tanggal_lahir = None
        if not no_telp:
            no_telp = None
        if not nama_lengkap:
            nama_lengkap = None
        
        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE no_ktp = %s", [no_ktp])
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["no_ktp"] = "Ktp is already registered"
        
        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE email = %s", [email])
        is_valid = len(cursor.fetchall()) == 0
        if not is_valid:
            error["email"] = "Email is already registered"

        if error:
            return render(request, 'user_register.html', {'error' : error})

        cursor.execute(
            """INSERT INTO PENGGUNA (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) 
            VALUES (%s, %s, %s, %s, %s)""",
            [no_ktp, nama_lengkap, email, tanggal_lahir, no_telp])
        
        if tipe == 'admin':
            cursor.execute(
                "INSERT INTO ADMIN (no_ktp) VALUES (%s)", [no_ktp])
        else:
            cursor.execute(
                "INSERT INTO ANGGOTA (no_ktp, poin, level) VALUES (%s, %s, %s)", [no_ktp, 0, "Bronze"])
            cursor.execute(
                """INSERT INTO ALAMAT (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) 
                VALUES (%s, %s, %s, %s, %s, %s)""",
                [no_ktp, nama_alamat, jalan, nomor, kota, kodepos])
        cursor.close()

        return render(request, 'user_login.html')

    return render(request, 'user_register.html')


def login(request):
    if request.method == 'GET':
        if "no_ktp" in request.session.keys():
            return redirect("/user/profile")
        return render(request, 'user_login.html')

    no_ktp = request.POST['no_ktp']
    email = request.POST['email']

    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM PENGGUNA WHERE no_ktp = %s AND email = %s", [no_ktp, email])

    is_valid = len(cursor.fetchall()) > 0
    if not is_valid:
        return render(request, 'user_login.html', {"error": "Login failed"})

    cursor.execute(
        "SELECT * FROM ADMIN WHERE no_ktp = %s", [no_ktp])
    is_admin = len(cursor.fetchall()) > 0

    cursor.close()

    request.session['no_ktp'] = no_ktp
    request.session['email'] = email
    request.session['is_admin'] = is_admin

    if is_admin:
        return redirect('/pesanan/')
    return redirect('/item/')


def logout(request):
    for key in list(request.session.keys()):
        del request.session[key]

    return redirect('/user/login')

@login_required
def profile(request, no_ktp, is_admin):
    cursor = connection.cursor()
    
    cursor.execute(
         "SELECT * FROM PENGGUNA WHERE no_ktp = %s", [no_ktp]
    )
    user = dictfetchall(cursor)[0]

    if not is_admin:
        cursor.execute(
            "SELECT * FROM ANGGOTA WHERE no_ktp = %s", [no_ktp]   
        )
        anggota = dictfetchall(cursor)[0]
        cursor.execute(
            "SELECT * FROM alamat WHERE no_ktp_anggota = %s", [no_ktp]
        )
        rows = dictfetchall(cursor)
        print(rows)
        alamats = []
        for row in rows:
            alamat = row['nama'] + " Jl." + row['jalan'] + " No." + str(row['nomor']) \
                    + ", Kota" +  row["kota"] + ", Kode Pos:" + row["kodepos"]
            alamats.append(alamat)



    cursor.close()

    user_data = {
        "no_ktp": user.get("no_ktp"),
        "nama_lengkap": user.get("nama_lengkap"),
        "email": user.get("email"),
        "tanggal_lahir": user.get("tanggal_lahir"),
        "no_telp": user.get("no_telp"),
        "is_admin": is_admin
    }

    if not is_admin:
        user_data["poin"] = anggota.get("poin")
        user_data["level"] = anggota.get("level")
        user_data["alamats"] = alamats

    return render(request, 'user_profile.html', {"user" : user_data})
