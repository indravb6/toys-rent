from django.shortcuts import redirect


def login_required(f):
    def g(request):
        if "no_ktp" not in request.session.keys():
            return redirect("/user/login")
        return f(request, request.session["no_ktp"], request.session["is_admin"])
    return g


def admin_required(f):
    def g(request):
        if "no_ktp" not in request.session.keys():
            return redirect("/user/login")
        if not request.session["is_admin"]:
            return redirect("/user/profile")
        return f(request, request.session["no_ktp"])
    return g
