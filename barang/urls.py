from django.urls import path
from .views import fetch_barang, add_barang, edit_barang, detail_barang, delete_barang

urlpatterns = [
    path('detail/', detail_barang, name='detail-barang'),
    path('edit/', edit_barang, name='edit-barang'),
    path('add/', add_barang, name='add-barang'),
    path('delete/', delete_barang, name='delete-barang'),
    path('', fetch_barang, name='fetch-barang'),
]
