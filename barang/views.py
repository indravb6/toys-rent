from django.shortcuts import render, redirect
from django.db import connection
from math import ceil

from user.helpers import login_required, admin_required
from toysrent.helpers import dictfetchall


@login_required
def fetch_barang(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if "page" in request.GET.keys():
        page = int(request.GET["page"]) - 1
    else:
        page = 0

    if "nama" in request.GET.keys():
        search_nama = '%' + request.GET["nama"] + '%'
    else:
        search_nama = '%%'

    order_by = request.GET.get("order_by", "id")
    order_method = request.GET.get("order_method", "ASC")

    cursor.execute(
        """SELECT CAST(id_barang AS int) as id, nama_item as nama, warna, kondisi FROM barang
        WHERE LOWER(nama_item) LIKE %s
        ORDER BY """ + order_by + " " + order_method + """ LIMIT 10 OFFSET %s""", [search_nama.lower(), 10 * page])
    rows = dictfetchall(cursor)
    cursor.execute("SELECT COUNT(1) as total_barang FROM barang WHERE LOWER(nama_item) LIKE %s", [
                   search_nama.lower()])
    total_barang = dictfetchall(cursor)[0].get("total_barang")
    total_page = ceil(total_barang / 10)
    cursor.close()

    data = {"all_barang": rows, "pages": range(1, total_page + 1), "page": page + 1, "is_admin": is_admin,
            "search_nama": search_nama[1:-1], "order_by": order_by, "order_method": order_method}

    return render(request, 'barang_fetch.html', data)


@admin_required
def add_barang(request, no_ktp):
    cursor = connection.cursor()

    cursor.execute(
        "SELECT nama_level FROM level_keanggotaan ORDER BY minimum_poin ASC")
    level_keanggotaan = [level.get("nama_level")
                         for level in dictfetchall(cursor)]

    if request.method == "POST":
        cursor.execute("INSERT INTO barang VALUES(%s, %s, %s, %s, %s, %s, %s)", [
            request.POST["id_barang"], request.POST["nama_item"], request.POST["warna"],
            request.POST["url_foto"], request.POST["kondisi"], request.POST["lama_penggunaan"],
            request.POST["no_ktp_penyewa"]
        ])

        for level in level_keanggotaan:
            harga_sewa = request.POST[level + "_harga_sewa"]
            porsi_royalti = request.POST[level + "_porsi_royalti"]
            cursor.execute("INSERT INTO info_barang_level VALUES (%s, %s, %s, %s)", [
                request.POST["id_barang"], level, harga_sewa, porsi_royalti
            ])

        return redirect("/barang/")

    cursor.execute("SELECT MAX(CAST(id_barang AS int)) as id FROM barang")
    next_id = dictfetchall(cursor)[0].get("id") + 1

    cursor.execute("SELECT nama FROM item")
    nama_item = [item.get("nama") for item in dictfetchall(cursor)]

    cursor.execute(
        "SELECT no_ktp, nama_lengkap FROM pengguna NATURAL JOIN anggota")
    daftar_anggota = dictfetchall(cursor)

    data = {"next_id": next_id, "level_keanggotaan": level_keanggotaan,
            "nama_item": nama_item, "daftar_anggota": daftar_anggota}

    return render(request, 'barang_add.html', data)


def get_detail_barang(id_barang):
    cursor = connection.cursor()
    cursor.execute(
        """SELECT * FROM barang
        JOIN pengguna ON barang.no_ktp_penyewa = pengguna.no_ktp
        WHERE id_barang = %s""", [id_barang]
    )
    row = dictfetchall(cursor)[0]
    cursor.execute(
        """SELECT * FROM info_barang_level
        NATURAL JOIN level_keanggotaan
        WHERE id_barang = %s
        ORDER BY minimum_poin DESC""",
        [id_barang]
    )
    info_barang = dictfetchall(cursor)
    info_barang = [
        {
            "nama_level": info.get("nama_level"),
            "harga_sewa": '{:,}'.format(int(info.get("harga_sewa"))),
            "harga_sewa_raw": int(info.get("harga_sewa")),
            "porsi_royalti": int(info.get("porsi_royalti") * 100)
        }
        for info in info_barang
    ]
    cursor.close()

    return {"data": row, "info_barang": info_barang}


@admin_required
def edit_barang(request, no_ktp):
    cursor = connection.cursor()

    cursor.execute(
        "SELECT nama_level FROM level_keanggotaan ORDER BY minimum_poin ASC")
    level_keanggotaan = [level.get("nama_level")
                         for level in dictfetchall(cursor)]

    if request.method == "POST":
        cursor.execute("""UPDATE barang SET
            nama_item = %s, warna = %s, url_foto = %s, kondisi = %s,
            lama_penggunaan = %s, no_ktp_penyewa = %s
            WHERE id_barang = %s""", [request.POST["nama_item"], request.POST["warna"], request.POST["url_foto"],
                                      request.POST["kondisi"], request.POST["lama_penggunaan"], request.POST[
                                          "no_ktp_penyewa"], request.POST["id_barang"]
                                      ])

        for level in level_keanggotaan:
            harga_sewa = request.POST[level + "_harga_sewa"]
            porsi_royalti = float(request.POST[level + "_porsi_royalti"]) / 100
            cursor.execute("""UPDATE info_barang_level SET
            harga_sewa = %s, porsi_royalti = %s WHERE 
            id_barang = %s AND nama_level = %s""", [
                harga_sewa, porsi_royalti, request.POST["id_barang"], level
            ])

        return redirect("/barang/edit/?id=" + request.POST["id_barang"])

    data = get_detail_barang(request.GET["id"])

    cursor.execute("SELECT nama FROM item")
    data["nama_item"] = [item.get("nama") for item in dictfetchall(cursor)]

    cursor.execute(
        "SELECT no_ktp, nama_lengkap FROM pengguna NATURAL JOIN anggota")
    data["daftar_anggota"] = dictfetchall(cursor)

    return render(request, 'barang_edit.html', data)


@login_required
def detail_barang(request, no_ktp, is_admin):
    data = get_detail_barang(request.GET["id"])
    return render(request, 'barang_detail.html', data)


@admin_required
def delete_barang(request, no_ktp):
    cursor = connection.cursor()
    cursor.execute(
        """DELETE FROM BARANG
        WHERE id_barang = %s""", [request.POST["deleteId"]]
    )

    return redirect("/barang/")
