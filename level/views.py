from django.shortcuts import render, redirect
from django.db import connection

from math import ceil

from user.helpers import login_required
from toysrent.helpers import dictfetchall

@login_required
def create_level(request, no_ktp, is_admin):
    cursor = connection.cursor()

    if(request.method == "POST"):
        print(request.POST)

        level = request.POST.get("level")
        deskripsi = request.POST.get("deskripsi")
        poin = request.POST.get("poin")

        cursor.execute("INSERT INTO level_keanggotaan VALUES(%s, %s, %s)", [
            level, poin, deskripsi
        ])

        return redirect("/level")
    return render(request, "create_level.html")


@login_required
def update_level(request, no_ktp, is_admin):
    cursor  = connection.cursor()
    data = {}

    if request.method == "POST":
        level = request.POST.get("level")
        poin = request.POST.get("poin")
        deskripsi = request.POST.get("deskripsi")
        cursor.execute("update level_keanggotaan set nama_level = '" + level + "' , minimum_poin = " + poin + ", deskripsi = '" + deskripsi + "' where nama_level = '" + level +"'")
        return redirect('/level')
    cursor  = connection.cursor()
    data = {}
    nama_level = request.GET.get("level")
    data["level"] = nama_level

    cursor.execute("select minimum_poin, deskripsi from level_keanggotaan where nama_level= '" + nama_level + "'")
    tmp = dictfetchall(cursor)

    data["poin"] = tmp[0]["minimum_poin"]
    data["deskripsi"] = tmp[0]["deskripsi"]
    return render(request, "update_level.html", data)


@login_required
def list_level(request, no_ktp, is_admin):
    if is_admin:
        data = {}
        cursor = connection.cursor()

        cursor.execute("SELECT nama_level, deskripsi, minimum_poin from level_keanggotaan")
        level = dictfetchall(cursor)
        i = 1
        for lev in level:
            lev["no"] = i
            i += 1

        data["levels"] = level

        return render(request, "list_level.html", data)
    else:
        return redirect("/")

@login_required
def delete_level(request, no_ktp, is_admin):

    nama_level = request.POST.get("level")
    cursor = connection.cursor()

    cursor.execute("delete from level_keanggotaan where nama_level = '" + nama_level + "'")
    return redirect('/level')
