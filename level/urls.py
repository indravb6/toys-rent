from django.urls import path

from .views import create_level, list_level, update_level, delete_level

urlpatterns = [
    path(r'level/delete', delete_level),
    path(r'level/create', create_level),
    path(r'level/update', update_level),
    path(r'level/', list_level),
]
