const show_register = (user) => {
    $("#tipe").val(`${user}`);
    $(".anggota-only").toggle();
    $(".register-button").removeClass("active");
    $(`.register-button-${user}`).addClass("active");
};
  
$(document).ready(() => {
    $(".register-button-admin").click();
});
