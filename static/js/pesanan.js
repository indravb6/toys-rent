function deletePesanan(id_pesanan) {
    $("#del-but").click(() => deletePesananFix(id_pesanan))
}


function deletePesananFix(id_pesanan) {
    $.ajax(
        {
            url: "/pesanan/delete",
            data : {"id_pemesanan" : id_pesanan, csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value },
            method : "POST",
            async: false,
        }
    )
    location.reload()
}

function openModal() {
    console.log("haha")
    $("#konfirmasiPesanan").modal("show");
}

function createPesanan(ktp, barang, waktu, harga) {
    console.log(ktp)
    console.log(barang)
    console.log(waktu)

    $.ajax(
        {
            url: "/pesanan/create",
            data : {"no_ktp" : ktp, "barang" : barang, "waktu": waktu, "harga" : harga, "confirmed": true, csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value },
            method : "POST",
            async: false,
        }
    )
    //document.location.href = "/pesanan"
}

function deleteLevel(level) {

    $("#del-level").click(() => deleteLevelFix(level))
}

function deleteLevelFix(level) {
    $.ajax(
        {
            url: "/level/delete",
            data : {"level" : level, csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value },
            method : "POST",
            async: false,
        }
    )
    location.reload()
}
