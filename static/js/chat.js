const show_chat = (id, no_ktp) => {
  $("#chat_to").val(no_ktp);
  $("#chat-panel").html($(`#chat-${id}`).html());
  $(".chat-button").removeClass("active");
  $(`.chat-button-${id}`).addClass("active");
};

$(document).ready(() => {
  $(".chat-button-1").click();
});
