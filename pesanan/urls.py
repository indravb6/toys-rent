from django.urls import path

from .views import create_pesanan, list_pesanan, update_pesanan,delete_pesanan

urlpatterns = [
    path(r'pesanan/create', create_pesanan),
    path(r'pesanan/update', update_pesanan),
    path(r'pesanan/', list_pesanan),
    path(r'pesanan/delete',delete_pesanan),
]
