from django.shortcuts import render, redirect
from django.db import connection
from datetime import datetime, timedelta

from math import ceil

from user.helpers import login_required
from toysrent.helpers import dictfetchall

# Create your views here.

@login_required
def create_pesanan(request, no_ktp, is_admin):
    cursor = connection.cursor()

    data = {}
    if (request.method == "POST"):
        confirmed = request.POST.get("confirmed")

        if confirmed:
            print("hahaha", no_ktp)
            kk = request.POST.get("no_ktp")
            print(kk)
            barang = request.POST.get("barang")
            waktu = request.POST.get("waktu")
            harga = request.POST.get("harga")

            cursor.execute("SELECT MAX(CAST(id_pemesanan AS int)) as id FROM pemesanan")
            next_id = dictfetchall(cursor)[0].get("id") + 1

            now = datetime.now() + timedelta(days = int(waktu))

            cursor.execute("INSERT INTO pemesanan VALUES(%s, %s, %s, %s, %s, %s, %s)", [
                next_id, datetime.now(), len(barang),
                int(harga), 0, kk,
                "Sedang Dikonfirmasi",
            ])

            cursor.execute("SELECT MAX(CAST(no_urut AS int)) as id FROM barang_pesanan")
            next_id_urut = dictfetchall(cursor)[0].get("id") + 1

            for bar in barang:
                cursor.execute("INSERT INTO barang_pesanan VALUES(%s, %s, %s, %s, %s, %s, %s)", [
                    next_id, next_id_urut, bar, datetime.now(), waktu, now, "Sedang Dikonfirmasi"
                ])
                next_id_urut += 1

            return redirect('/pesanan/')

        no_ktp = request.POST.get("anggota", no_ktp)
        list_barang = request.POST.get("barang")
        waktu = request.POST.get("waktu")
        data["ktp"] = no_ktp
        data["list_barang"] = list_barang
        data["waktu"] = waktu

        error = ""
        cursor.execute("select no_ktp from anggota where no_ktp= '" + no_ktp + "'");
        tmp = dictfetchall(cursor)
        if(not tmp):
            error = "No_ktp tidak valid. "

        try:
            tmp = int(waktu)
        except:
            error = "Lama sewa tidak valid."

        if error:
            data["error"] = error
        else:
            total = 0
            for barang in list_barang:
                cursor.execute("select harga_sewa from info_barang_level, anggota where id_barang = '" + barang + "' and anggota.no_ktp = '" + no_ktp +"' and info_barang_level.nama_level = anggota.level")
                #print(dictfetchall(cursor), barang)
                total += int(dictfetchall(cursor)[0]["harga_sewa"])
            print(no_ktp)
            data["total"] = total
    if is_admin:
        cursor.execute("select no_ktp from anggota");
        tmp = dictfetchall(cursor)
        data["no_ktp"] = tmp
    else:
        data["no_ktp"] = no_ktp

    cursor.execute("select nama_item, id_barang from barang")
    barang = dictfetchall(cursor)

    data["barang"] = barang
    data["is_admin"] = is_admin
    print(data)
    return render(request, 'create_pesanan.html', data)

@login_required
def update_pesanan(request, no_ktp, is_admin):
    cursor = connection.cursor()
    if(request.method == "POST"):
        id = request.POST.get("id")
        status = request.POST.get("status")
        lama = request.POST.get("lama")
        print(request.POST)

        cursor.execute("update pemesanan set status='" + status + "' where id_pemesanan = '" + id + "';")

        cursor.execute("update barang_pesanan set nama_status='" + status + "', lama_sewa = '" + lama  +"' where id_pemesanan = '" + id + "';")

        return redirect("/pesanan")
    id_barang = request.GET.get("id")
    data = {}
    data["id_pemesanan"] = id_barang
    data["is_admin"] = is_admin

    cursor.execute("select B.nama_item, BP.lama_sewa, P.status from Barang B, Pemesanan P, Barang_Pesanan BP where P.id_pemesanan='" + id_barang + "'   and BP.id_pemesanan = P.id_pemesanan and BP.id_barang = B.id_barang");
    barang = dictfetchall(cursor)

    if(not barang):
        return redirect("/pesanan")

    lama = barang[0]["lama_sewa"]
    nama_barang = ""
    for bar in barang:
        nama_barang += bar["nama_item"] + ", "

    data["status"] = barang[0]["status"]
    data["barang"] = nama_barang
    data["lama"] = lama


    cursor.execute("Select nama from status");
    data["all_status"] = dictfetchall(cursor)


    return render(request, 'update_pesanan.html', data)

@login_required
def list_pesanan(request, no_ktp, is_admin):
    page = int(request.GET.get('page', 1))

    cursor = connection.cursor()


    filt = request.GET.get('filter', "")
    filt = " id_pemesanan like '%%" + filt + "%%'";

    print(filt)

    if is_admin:
        cursor.execute("select cast(id_pemesanan as int), harga_sewa + ongkos as harga, status from pemesanan where" + filt + " order by id_pemesanan");
    else :
        cursor.execute("select cast(id_pemesanan as int), harga_sewa + ongkos as harga, status from pemesanan where no_ktp_pemesan='"+no_ktp+"' and" + filt + " order by id_pemesanan");
    data = dictfetchall(cursor)

    max_page = ceil(len(data) / 10)
    page = min(page, max_page)
    page = max(1, page)

    data = data[10 * (page-1) : 10 * page]

    for pesanan in data:
        print(pesanan)
        cursor.execute("select B.nama_item from Barang B, Pemesanan P, Barang_Pesanan BP where P.id_pemesanan='" + str(pesanan["id_pemesanan"]) + "'   and BP.id_pemesanan = P.id_pemesanan and BP.id_barang = B.id_barang");
        barang = dictfetchall(cursor)
        pesanan["barang"] = barang

    return render(request, "list_pesanan.html", {"data": data, "first_page" : (page == 1), "last_page" : (page == max_page), "next_page" : page + 1, "prev_page": page-1, "is_admin": is_admin})

@login_required
def delete_pesanan(request, no_ktp, is_admin):
    id_pesanan = request.POST.get('id_pemesanan')

    cursor = connection.cursor()

    cursor.execute("delete from pemesanan p where p.id_pemesanan = '" + str(id_pesanan) + "'")
