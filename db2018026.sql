--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 10.8 (Ubuntu 10.8-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: toys_rent; Type: SCHEMA; Schema: -; Owner: db2018026
--

CREATE SCHEMA toys_rent;


ALTER SCHEMA toys_rent OWNER TO db2018026;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hitung_harga_sewa(); Type: FUNCTION; Schema: toys_rent; Owner: db2018026
--

CREATE FUNCTION toys_rent.hitung_harga_sewa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    level_pengguna VARCHAR;
    harga_sewa_barang INTEGER;
    harga_sewa_prev INTEGER;
  BEGIN
    SELECT Level INTO level_pengguna FROM PEMESANAN A
    JOIN ANGGOTA B ON B.No_ktp = A.No_ktp_pemesan
    WHERE A.Id_pemesanan = NEW.Id_pemesanan;

    SELECT Harga_sewa INTO harga_sewa_barang FROM INFO_BARANG_LEVEL
    WHERE Id_barang = NEW.Id_barang AND Nama_level = level_pengguna;

    SELECT Harga_sewa INTO harga_sewa_prev FROM PEMESANAN
    WHERE Id_pemesanan = NEW.Id_pemesanan;

    UPDATE PEMESANAN SET Harga_sewa = harga_sewa_prev + harga_sewa_barang
    WHERE Id_pemesanan = NEW.Id_pemesanan;

    RETURN NEW;
  END;
$$;



ALTER FUNCTION toys_rent.hitung_harga_sewa() OWNER TO db2018026;

--
-- Name: hitung_ongkos(); Type: FUNCTION; Schema: toys_rent; Owner: db2018026
--

CREATE FUNCTION toys_rent.hitung_ongkos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    ongkos_prev FLOAT;
  BEGIN
    SELECT Ongkos INTO ongkos_prev FROM PEMESANAN
    WHERE Id_pemesanan = NEW.Id_pemesanan;

    UPDATE PEMESANAN SET Ongkos = ongkos_prev + NEW.Ongkos
    WHERE Id_pemesanan = NEW.Id_pemesanan;

    RETURN NEW;
  END;
$$;


ALTER FUNCTION toys_rent.hitung_ongkos() OWNER TO db2018026;

--
-- Name: update_kondisi_barang_pesanan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018026
--

CREATE FUNCTION toys_rent.update_kondisi_barang_pesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
update barang
set kondisi = 'Sedang disewa anggota'
where NEW.id_barang = barang.id_barang;
return new;
end;
$$;


ALTER FUNCTION toys_rent.update_kondisi_barang_pesanan() OWNER TO db2018026;

--
-- Name: update_level_anggota(); Type: FUNCTION; Schema: toys_rent; Owner: db2018026
--

CREATE OR REPLACE FUNCTION toys_rent.update_level_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        temp_row RECORD;
        new_level CHARACTER varying(20);

    BEGIN
        IF(TG_OP='UPDATE' AND NEW=OLD) THEN
   RETURN NEW;
  END IF;

        FOR temp_row IN
            SELECT a.no_ktp, MAX(l.minimum_poin) as minimum_poin
            FROM ANGGOTA a, LEVEL_KEANGGOTAAN l
            WHERE a.poin >= l.minimum_poin
            GROUP BY a.no_ktp
        LOOP
            SELECT b.nama_level INTO new_level
            FROM LEVEL_KEANGGOTAAN b
            WHERE b.minimum_poin = temp_row.minimum_poin;

            UPDATE ANGGOTA a
                SET level = new_level
                WHERE a.no_ktp = temp_row.no_ktp;
        END LOOP;

        RETURN NEW;
    END;
$$;


ALTER FUNCTION toys_rent.update_level_anggota() OWNER TO db2018026;

--
-- Name: update_point_insert_barang(); Type: FUNCTION; Schema: toys_rent; Owner: db2018026
--

CREATE FUNCTION toys_rent.update_point_insert_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
UPDATE ANGGOTA A
SET poin = poin + 100
WHERE A.no_ktp = NEW.no_ktp_penyewa;
RETURN NEW;
END;
$$;


ALTER FUNCTION toys_rent.update_point_insert_barang() OWNER TO db2018026;

--
-- Name: update_point_pemesanan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018026
--

CREATE OR REPLACE FUNCTION
update_point_pemesanan()
RETURNS TRIGGER AS
$$
 DECLARE

 BEGIN
  IF(TG_OP='INSERT') THEN
   UPDATE ANGGOTA A
   SET poin = poin + NEW.kuantitas_barang * 100
   WHERE A.no_ktp = NEW.no_ktp_pemesan;
   RETURN NEW;
  END IF;

  IF(TG_OP='UPDATE') THEN
   UPDATE ANGGOTA A
   SET poin = poin - OLD.kuantitas_barang * 100 + NEW.kuantitas_barang * 100
   WHERE A.no_ktp = NEW.no_ktp_pemesan;
   RETURN NEW;
  END IF;

  IF(TG_OP='DELETE') THEN
   UPDATE ANGGOTA A
   SET poin = poin - OLD.kuantitas_barang * 100
   WHERE A.no_ktp = OLD.no_ktp_pemesan;
   RETURN NEW;
  END IF;

 END;
$$
LANGUAGE plpgsql;


ALTER FUNCTION toys_rent.update_point_pemesanan() OWNER TO db2018026;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE toys_rent.admin OWNER TO db2018026;

--
-- Name: alamat; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);


ALTER TABLE toys_rent.alamat OWNER TO db2018026;

--
-- Name: anggota; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


ALTER TABLE toys_rent.anggota OWNER TO db2018026;

--
-- Name: barang; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20) NOT NULL
);


ALTER TABLE toys_rent.barang OWNER TO db2018026;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10)
);


ALTER TABLE toys_rent.barang_dikembalikan OWNER TO db2018026;

--
-- Name: barang_dikirim; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL
);


ALTER TABLE toys_rent.barang_dikirim OWNER TO db2018026;

--
-- Name: barang_pesanan; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10) NOT NULL,
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    nama_status character varying(50)
);


ALTER TABLE toys_rent.barang_pesanan OWNER TO db2018026;

--
-- Name: chat; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


ALTER TABLE toys_rent.chat OWNER TO db2018026;

--
-- Name: info_barang_level; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_royalti real NOT NULL
);


ALTER TABLE toys_rent.info_barang_level OWNER TO db2018026;

--
-- Name: item; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);


ALTER TABLE toys_rent.item OWNER TO db2018026;

--
-- Name: kategori; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.kategori (
    nama character varying(255) NOT NULL,
    level integer NOT NULL,
    sub_dari character varying(255)
);


ALTER TABLE toys_rent.kategori OWNER TO db2018026;

--
-- Name: kategori_item; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


ALTER TABLE toys_rent.kategori_item OWNER TO db2018026;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE toys_rent.level_keanggotaan OWNER TO db2018026;

--
-- Name: pemesanan; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


ALTER TABLE toys_rent.pemesanan OWNER TO db2018026;

--
-- Name: pengembalian; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toys_rent.pengembalian OWNER TO db2018026;

--
-- Name: pengguna; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE toys_rent.pengguna OWNER TO db2018026;

--
-- Name: pengiriman; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toys_rent.pengiriman OWNER TO db2018026;

--
-- Name: status; Type: TABLE; Schema: toys_rent; Owner: db2018026
--

CREATE TABLE toys_rent.status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


ALTER TABLE toys_rent.status OWNER TO db2018026;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.admin (no_ktp) FROM stdin;
08301252739722567087
73315370623766663997
99209299096497566156
14819673651963931895
85871230537203328052
03776350565147233050
64240430504127626817
50035056544919604759
24032874520349018291
95155846837355220109
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
54368352265519664000	Rumah	Monterey	5	Póvoa de Penela	17646
69406617935543459583	Kantor	Hanson	15	Duma	53955
02042048624558529710	Rumah	Kedzie	27	Caoxi	28090
34021506316616141151	Kantor	Atwood	27	Pohbango	44988
53196963634942900541	Rumah	Ilene	6	Pelaya	85486
03829953029916687911	Kantor	3rd	17	Xindian	62736
81686336623645601283	Rumah	Northport	23	Qianpai	65026
09054979656025876738	Kantor	Brown	12	Lima Pampa	14077
79637781754587917528	Rumah	Goodland	30	Tuchkovo	95175
91431208368334322216	Kantor	Veith	17	Mohoro	33333
52148364448001622107	Rumah	Mallory	14	Alegre	81686
37035872630850054065	Kantor	Grasskamp	4	Narail	61639
21594813884892270596	Rumah	Crowley	27	San Jacinto	21729
73672218364347144165	Kantor	Clarendon	22	Dongchuan	72241
87147487624650243570	Rumah	Kropf	5	Ruy Barbosa	49208
73605563756248532873	Kantor	Raven	16	Gorzyce	52936
73506622399156528946	Rumah	8th	17	Kademangan	28236
89031875319756622272	Kantor	Randy	5	Los Angeles	42334
40243728783507793290	Rumah	Golf	6	Ilanskiy	19936
08325231543252789834	Kantor	Merrick	30	Bureng	36540
98805058936524648004	Rumah	Gateway	9	Adare	15596
93995649247613633399	Kantor	Westport	6	Killorglin	35500
58123227991230660569	Rumah	Roxbury	1	Rzhev	37183
72412300250595634568	Kantor	Loomis	30	Weishan	12391
15132931449404851787	Rumah	Village Green	15	Zakan-Yurt	30360
67409469193068326386	Kantor	Comanche	15	Sade	75999
60820969271826157051	Rumah	7th	28	Kleszczów	62071
36840789280324764449	Kantor	Westport	23	Havre-Saint-Pierre	81846
09326281851165173221	Rumah	Monica	2	Kudang	73397
73249788789082064681	Kantor	Kings	5	Caringin Lor	58893
61635483574180442192	Rumah	Bluestem	12	Jiangbei	29036
39911274904286030651	Kantor	Chive	9	Wlingi	80738
35402646113236398928	Rumah	Redwing	2	Uspenskoye	86097
42140288875353525504	Kantor	Bunting	17	Rýmařov	39551
95849046839115573948	Rumah	Lawn	30	Buayan	72376
09643404999667107947	Kantor	Lakewood	26	Menghai	89623
28946547154693336808	Rumah	Arapahoe	8	Raszków	61095
19840600485216889454	Kantor	Cascade	10	Öldziyt	68937
42110701225884818282	Rumah	Cardinal	21	Latung	12911
42433278063120933230	Kantor	Mcbride	3	Ciusul	38554
98652913552510199993	Rumah	Walton	22	Ciomas	18489
85938622154195306824	Kantor	Schlimgen	3	Chingas	28890
45166979530846509458	Rumah	8th	13	Concepcion	46333
68528283622701707948	Kantor	Farmco	1	Przemyśl	14576
97009377697291677089	Rumah	Mesta	8	Merlo	41495
63562589239122852947	Kantor	Cottonwood	16	Totora	39760
78014214586158244148	Rumah	Anniversary	27	Krajan Battal	32928
42931284178733134589	Kantor	Weeping Birch	18	Independencia	18624
69807569402409459211	Rumah	Ramsey	27	Kaura Namoda	58800
10160532657929709480	Kantor	South	3	Skuratovskiy	28389
60171449186896735313	Rumah	Forest Run	2	Concepción	63671
52752754268141846440	Kantor	Merchant	6	Nahiyat Ghammas	76213
20948543328395218639	Rumah	Canary	20	Stockholm	24734
05231756515975385982	Kantor	Scofield	6	Espinheira	68260
66025996395372277203	Rumah	Cordelia	8	Bartolomé Masó	66628
93741652064609229097	Kantor	Glacier Hill	18	Nāḩiyat as Sab‘ Biyār	43624
69498951289378631191	Rumah	Del Mar	22	Sivaki	40487
72905554931015048977	Kantor	Main	20	Iwata	83452
19948537697376309029	Rumah	Oxford	9	Hezuoqiao	92471
92765419216039307203	Kantor	Muir	20	Caringin	62383
50929912452266775152	Rumah	Gale	28	San Pedro	96804
65239212634009723486	Kantor	Quincy	24	Banjar Kampungbugis	96086
83170353179132776193	Rumah	Bayside	11	San Miguel de Cauri	86693
72472468568913161583	Kantor	Debs	23	Poli	56076
46835775042474062710	Rumah	Holy Cross	1	Tambo	31217
38004888765418454368	Kantor	Scott	21	Jacareí	97092
86203763546087351236	Rumah	Eagle Crest	23	Yaozhuang	97210
21255518690756411396	Kantor	Superior	4	Longxian	70449
28446781511455104547	Rumah	Rowland	17	Qo‘qon	24980
98173357558824190885	Kantor	Eagle Crest	1	Petkovci	42827
07639382540165082999	Rumah	Jenifer	6	Kstovo	72945
16312505491989204658	Kantor	Cordelia	21	Borek Wielkopolski	51748
85581315551970404633	Rumah	Fisk	1	Kumo	44624
14165759167360865502	Kantor	Talmadge	17	Namballe	55776
43839378479279291640	Rumah	Anthes	19	Tomelilla	96999
63510126440951680126	Kantor	Veith	28	Pagsañgahan	95465
66315564854348311821	Rumah	Rusk	16	Takahata	63580
40943312463493083803	Kantor	Brickson Park	24	Manuel Cavazos Lerma	70507
73734061093203616225	Rumah	7th	17	Lapuz	91099
72020110548056782343	Kantor	Cambridge	9	Xieba	68227
89740790380502631922	Rumah	Acker	24	Mashizhai	54247
23240755615582069981	Kantor	East	5	Balta	93831
97174072491836012462	Rumah	Fieldstone	25	La Unión	31309
03804228984688684410	Kantor	Mosinee	12	Zhongmen	63173
56638514343485692856	Rumah	Fisk	6	Maddarulug Norte	32886
74024871534734781148	Kantor	Lunder	28	Jalaud	75538
88500163593013419402	Rumah	Ryan	2	Dapdapan	13737
99807022136199706322	Kantor	Chive	21	San Javier	62128
33865363860970895904	Rumah	Mcguire	29	Stare Kurowo	14667
68649091168848100982	Kantor	Sycamore	21	San Antonio	40134
54368352265519664000	Kantor	Macpherson	21	Heerlen	13788
69406617935543459583	Rumah	Prairie Rose	30	Padre Bernardo	97191
02042048624558529710	Kantor	Florence	29	Munduk	45060
34021506316616141151	Rumah	2nd	7	Strašice	71058
53196963634942900541	Kantor	Lawn	16	Bāsht	70974
03829953029916687911	Rumah	Walton	20	Palmital	21720
81686336623645601283	Kantor	Center	29	Pejukutan	30116
09054979656025876738	Rumah	Florence	13	Brändö	81145
79637781754587917528	Kantor	Vermont	26	Xudat	85428
91431208368334322216	Rumah	Service	28	Qingfeng	34839
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.anggota (no_ktp, poin, level) FROM stdin;
54368352265519664000	23	Bronze
69406617935543459583	88	Gold
02042048624558529710	33	Bronze
34021506316616141151	71	Silver
53196963634942900541	56	Silver
03829953029916687911	40	Bronze
81686336623645601283	86	Gold
09054979656025876738	76	Silver
79637781754587917528	37	Bronze
91431208368334322216	42	Bronze
52148364448001622107	23	Bronze
37035872630850054065	19	Bronze
21594813884892270596	70	Silver
73672218364347144165	57	Silver
87147487624650243570	54	Silver
73605563756248532873	67	Silver
73506622399156528946	25	Bronze
89031875319756622272	18	Bronze
40243728783507793290	55	Silver
08325231543252789834	21	Bronze
98805058936524648004	66	Silver
93995649247613633399	84	Gold
58123227991230660569	71	Silver
72412300250595634568	49	Bronze
15132931449404851787	16	Bronze
67409469193068326386	9	Bronze
60820969271826157051	54	Silver
36840789280324764449	69	Silver
09326281851165173221	73	Silver
73249788789082064681	66	Silver
61635483574180442192	58	Silver
39911274904286030651	52	Silver
35402646113236398928	80	Gold
42140288875353525504	23	Bronze
95849046839115573948	33	Bronze
09643404999667107947	31	Bronze
28946547154693336808	62	Silver
19840600485216889454	12	Bronze
42110701225884818282	26	Bronze
42433278063120933230	43	Bronze
98652913552510199993	85	Gold
85938622154195306824	25	Bronze
45166979530846509458	9	Bronze
68528283622701707948	43	Bronze
97009377697291677089	63	Silver
63562589239122852947	72	Silver
78014214586158244148	72	Silver
42931284178733134589	30	Bronze
69807569402409459211	47	Bronze
10160532657929709480	65	Silver
60171449186896735313	77	Silver
52752754268141846440	44	Bronze
20948543328395218639	27	Bronze
05231756515975385982	62	Silver
66025996395372277203	63	Silver
93741652064609229097	69	Silver
69498951289378631191	10	Bronze
72905554931015048977	75	Silver
19948537697376309029	3	Bronze
92765419216039307203	39	Bronze
50929912452266775152	63	Silver
65239212634009723486	45	Bronze
83170353179132776193	86	Gold
72472468568913161583	67	Silver
46835775042474062710	18	Bronze
38004888765418454368	35	Bronze
86203763546087351236	5	Bronze
21255518690756411396	19	Bronze
28446781511455104547	49	Bronze
98173357558824190885	7	Bronze
07639382540165082999	71	Silver
16312505491989204658	54	Silver
85581315551970404633	77	Silver
14165759167360865502	71	Silver
43839378479279291640	81	Gold
63510126440951680126	36	Bronze
40943312463493083803	70	Silver
73734061093203616225	29	Bronze
72020110548056782343	89	Gold
89740790380502631922	49	Bronze
23240755615582069981	26	Bronze
97174072491836012462	22	Bronze
03804228984688684410	59	Silver
56638514343485692856	70	Silver
74024871534734781148	68	Silver
88500163593013419402	14	Bronze
99807022136199706322	64	Silver
33865363860970895904	16	Bronze
68649091168848100982	35	Bronze
66315564854348311821	53	Silver
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
1	Rubik Pyraminx	Mauv	http://dummyimage.com/180x168.png/cc0000/ffffff	Baik	27	19948537697376309029
2	Lego	Goldenrod	http://dummyimage.com/182x240.png/dddddd/000000	Kinclong	25	42931284178733134589
4	Jigsaw Sedang	Purple	http://dummyimage.com/191x223.bmp/cc0000/ffffff	Mulus	14	95849046839115573948
5	Biola	Indigo	http://dummyimage.com/149x203.jpg/cc0000/ffffff	Mulus	7	60820969271826157051
6	Rubik 3x3	Fuscia	http://dummyimage.com/210x232.png/ff4444/ffffff	Lama	1	92765419216039307203
7	Kaos Kaki	Purple	http://dummyimage.com/228x156.png/5fa2dd/ffffff	Mulus	25	68528283622701707948
8	Spongebob	Teal	http://dummyimage.com/161x134.jpg/ff4444/ffffff	Baru	15	67409469193068326386
9	Jigsaw Besar	Khaki	http://dummyimage.com/211x129.jpg/dddddd/000000	Mulus	3	88500163593013419402
10	Origami	Puce	http://dummyimage.com/231x209.bmp/cc0000/ffffff	Baru	23	98652913552510199993
11	Rumah-rumahan	Puce	http://dummyimage.com/210x143.jpg/ff4444/ffffff	Kinclong	9	37035872630850054065
12	Bando	Maroon	http://dummyimage.com/174x207.png/5fa2dd/ffffff	Kurang Baik	13	38004888765418454368
13	Mahkota	Khaki	http://dummyimage.com/141x207.png/5fa2dd/ffffff	Baru	17	33865363860970895904
14	Hotwheels	Yellow	http://dummyimage.com/233x132.jpg/cc0000/ffffff	Lama	26	88500163593013419402
15	Naruto	Teal	http://dummyimage.com/103x155.jpg/dddddd/000000	Kinclong	16	10160532657929709480
16	Kaos Kaki	Mauv	http://dummyimage.com/215x183.bmp/5fa2dd/ffffff	Kurang Baik	19	33865363860970895904
17	Lego	Red	http://dummyimage.com/248x146.bmp/dddddd/000000	Kurang Baik	25	53196963634942900541
18	Rubik Pyraminx	Teal	http://dummyimage.com/196x179.png/cc0000/ffffff	Kurang Baik	28	78014214586158244148
19	Baju	Pink	http://dummyimage.com/135x221.jpg/dddddd/000000	Kinclong	10	73249788789082064681
20	Naruto	Crimson	http://dummyimage.com/162x205.png/dddddd/000000	Baik	4	03829953029916687911
21	Suling	Blue	http://dummyimage.com/187x204.png/ff4444/ffffff	Kurang Baik	29	09643404999667107947
22	Origami	Indigo	http://dummyimage.com/125x125.jpg/cc0000/ffffff	Baru	20	74024871534734781148
23	Nesoberi	Indigo	http://dummyimage.com/192x166.bmp/cc0000/ffffff	Kurang Baik	24	72020110548056782343
24	Asuna	Teal	http://dummyimage.com/151x148.bmp/5fa2dd/ffffff	Mulus	16	37035872630850054065
25	Spongebob	Teal	http://dummyimage.com/153x241.jpg/5fa2dd/ffffff	Mulus	13	74024871534734781148
26	Hotwheels	Turquoise	http://dummyimage.com/158x101.png/ff4444/ffffff	Baru	9	68649091168848100982
27	Spongebob	Mauv	http://dummyimage.com/145x136.jpg/5fa2dd/ffffff	Kurang Baik	15	85938622154195306824
28	Biola	Crimson	http://dummyimage.com/202x242.png/dddddd/000000	Kinclong	19	42110701225884818282
29	Baju	Orange	http://dummyimage.com/171x159.bmp/cc0000/ffffff	Kurang Baik	22	28446781511455104547
30	Rubik 4x4	Teal	http://dummyimage.com/104x192.png/ff4444/ffffff	Kinclong	21	98173357558824190885
31	Chucky	Goldenrod	http://dummyimage.com/123x185.jpg/5fa2dd/ffffff	Lama	23	07639382540165082999
32	Ayunan	Blue	http://dummyimage.com/159x185.bmp/ff4444/ffffff	Lama	3	73506622399156528946
33	Piano	Aquamarine	http://dummyimage.com/217x239.png/5fa2dd/ffffff	Kinclong	4	03804228984688684410
34	Jigsaw Besar	Green	http://dummyimage.com/234x192.bmp/cc0000/ffffff	Lama	21	72472468568913161583
35	Lego	Puce	http://dummyimage.com/186x100.png/5fa2dd/ffffff	Kurang Baik	22	66025996395372277203
36	Chucky	Goldenrod	http://dummyimage.com/226x249.png/cc0000/ffffff	Kurang Baik	19	72020110548056782343
37	Jigsaw Besar	Goldenrod	http://dummyimage.com/104x161.png/cc0000/ffffff	Mulus	9	39911274904286030651
38	Bando	Maroon	http://dummyimage.com/186x172.bmp/cc0000/ffffff	Baik	12	53196963634942900541
39	Jigsaw Sedang	Aquamarine	http://dummyimage.com/122x183.jpg/dddddd/000000	Mulus	5	23240755615582069981
40	Jigsaw Kecil	Yellow	http://dummyimage.com/112x216.bmp/5fa2dd/ffffff	Kurang Baik	6	35402646113236398928
41	Rubik Pyraminx	Indigo	http://dummyimage.com/187x184.png/dddddd/000000	Kinclong	8	95849046839115573948
42	Celana	Red	http://dummyimage.com/248x222.bmp/ff4444/ffffff	Baru	16	28946547154693336808
43	Bando	Purple	http://dummyimage.com/197x174.bmp/dddddd/000000	Mulus	8	98652913552510199993
44	Rubik 2x2	Turquoise	http://dummyimage.com/212x101.bmp/ff4444/ffffff	Baik	20	42931284178733134589
45	Piano	Crimson	http://dummyimage.com/202x142.bmp/dddddd/000000	Lama	15	72020110548056782343
46	Rubik Pyraminx	Indigo	http://dummyimage.com/182x143.bmp/cc0000/ffffff	Baru	7	42140288875353525504
47	Asuna	Goldenrod	http://dummyimage.com/128x204.bmp/cc0000/ffffff	Lama	3	40243728783507793290
48	Kaos Kaki	Green	http://dummyimage.com/228x104.jpg/ff4444/ffffff	Lama	7	36840789280324764449
49	Rubik 2x2	Goldenrod	http://dummyimage.com/224x120.png/5fa2dd/ffffff	Baru	25	56638514343485692856
50	Kaos Kaki	Turquoise	http://dummyimage.com/203x106.png/dddddd/000000	Baru	10	39911274904286030651
51	Gitar	Turquoise	http://dummyimage.com/125x248.jpg/5fa2dd/ffffff	Kurang Baik	29	19840600485216889454
52	Hatsune Miku	Pink	http://dummyimage.com/220x244.bmp/ff4444/ffffff	Baru	16	69807569402409459211
53	Asuna	Maroon	http://dummyimage.com/191x247.bmp/ff4444/ffffff	Mulus	10	19948537697376309029
54	Bando	Blue	http://dummyimage.com/240x160.bmp/5fa2dd/ffffff	Kinclong	6	10160532657929709480
55	Kaos Kaki	Teal	http://dummyimage.com/240x190.bmp/dddddd/000000	Kurang Baik	10	09326281851165173221
56	Plastisin	Red	http://dummyimage.com/221x161.png/cc0000/ffffff	Mulus	3	86203763546087351236
57	Lego	Blue	http://dummyimage.com/149x202.jpg/5fa2dd/ffffff	Lama	11	73605563756248532873
58	Bass	Blue	http://dummyimage.com/105x166.jpg/ff4444/ffffff	Baru	17	72020110548056782343
59	Rubik 4x4	Khaki	http://dummyimage.com/135x150.bmp/5fa2dd/ffffff	Baru	10	69498951289378631191
60	Chucky	Goldenrod	http://dummyimage.com/107x244.png/ff4444/ffffff	Lama	10	33865363860970895904
61	Hotwheels	Violet	http://dummyimage.com/173x198.jpg/dddddd/000000	Baik	5	42110701225884818282
62	Naruto	Indigo	http://dummyimage.com/101x123.png/cc0000/ffffff	Kinclong	16	99807022136199706322
63	Ayunan	Pink	http://dummyimage.com/173x163.jpg/dddddd/000000	Kurang Baik	23	02042048624558529710
64	Suling	Orange	http://dummyimage.com/150x140.png/cc0000/ffffff	Baru	3	21255518690756411396
65	Asuna	Khaki	http://dummyimage.com/106x201.png/cc0000/ffffff	Kurang Baik	11	16312505491989204658
66	Sasuke	Maroon	http://dummyimage.com/212x140.jpg/ff4444/ffffff	Baru	13	97174072491836012462
67	Rubik 2x2	Yellow	http://dummyimage.com/192x238.jpg/5fa2dd/ffffff	Mulus	30	97174072491836012462
68	Bass	Fuscia	http://dummyimage.com/103x193.jpg/ff4444/ffffff	Mulus	29	67409469193068326386
69	Suling	Mauv	http://dummyimage.com/211x118.bmp/dddddd/000000	Kurang Baik	11	35402646113236398928
70	Sasuke	Orange	http://dummyimage.com/169x243.png/ff4444/ffffff	Baru	6	02042048624558529710
71	Hatsune Miku	Puce	http://dummyimage.com/129x122.jpg/5fa2dd/ffffff	Lama	15	73734061093203616225
72	Sasuke	Blue	http://dummyimage.com/240x123.png/cc0000/ffffff	Baik	1	21594813884892270596
73	Celana	Mauv	http://dummyimage.com/191x131.png/cc0000/ffffff	Kinclong	13	35402646113236398928
74	Origami	Fuscia	http://dummyimage.com/128x193.bmp/5fa2dd/ffffff	Lama	1	40243728783507793290
75	Jigsaw Kecil	Indigo	http://dummyimage.com/148x113.png/ff4444/ffffff	Baru	5	66315564854348311821
76	Jigsaw Sedang	Crimson	http://dummyimage.com/231x142.png/cc0000/ffffff	Kurang Baik	30	07639382540165082999
77	Naruto	Orange	http://dummyimage.com/141x236.png/cc0000/ffffff	Lama	7	87147487624650243570
78	Jigsaw Besar	Khaki	http://dummyimage.com/235x223.bmp/ff4444/ffffff	Kurang Baik	2	73672218364347144165
79	Rumah-rumahan	Turquoise	http://dummyimage.com/249x129.png/ff4444/ffffff	Kurang Baik	15	42140288875353525504
80	Asuna	Pink	http://dummyimage.com/173x211.jpg/ff4444/ffffff	Mulus	20	16312505491989204658
81	Lego	Teal	http://dummyimage.com/202x220.bmp/5fa2dd/ffffff	Kinclong	27	61635483574180442192
82	Hatsune Miku	Pink	http://dummyimage.com/230x150.png/dddddd/000000	Mulus	1	73506622399156528946
83	Piano	Purple	http://dummyimage.com/210x216.jpg/cc0000/ffffff	Lama	6	63510126440951680126
84	Hotwheels	Maroon	http://dummyimage.com/185x102.png/cc0000/ffffff	Lama	21	66315564854348311821
85	Plastisin	Violet	http://dummyimage.com/126x136.jpg/ff4444/ffffff	Mulus	1	79637781754587917528
86	Suling	Turquoise	http://dummyimage.com/227x239.png/dddddd/000000	Kurang Baik	13	50929912452266775152
87	Plastisin	Red	http://dummyimage.com/222x158.jpg/ff4444/ffffff	Kinclong	7	35402646113236398928
88	Hotwheels	Yellow	http://dummyimage.com/231x205.bmp/ff4444/ffffff	Lama	11	78014214586158244148
89	Ayunan	Khaki	http://dummyimage.com/102x177.png/ff4444/ffffff	Kinclong	18	52752754268141846440
90	Jigsaw Sedang	Orange	http://dummyimage.com/162x214.bmp/ff4444/ffffff	Kurang Baik	20	98173357558824190885
91	Nesoberi	Indigo	http://dummyimage.com/113x242.png/ff4444/ffffff	Kinclong	12	69406617935543459583
92	Biola	Turquoise	http://dummyimage.com/104x166.bmp/cc0000/ffffff	Kurang Baik	14	97009377697291677089
93	Kaos Kaki	Indigo	http://dummyimage.com/194x242.bmp/cc0000/ffffff	Mulus	22	54368352265519664000
94	Nesoberi	Aquamarine	http://dummyimage.com/115x239.jpg/5fa2dd/ffffff	Kinclong	4	23240755615582069981
95	Mahkota	Red	http://dummyimage.com/161x173.jpg/cc0000/ffffff	Kinclong	4	66315564854348311821
96	Hatsune Miku	Red	http://dummyimage.com/145x117.png/5fa2dd/ffffff	Baik	30	93995649247613633399
97	Naruto	Goldenrod	http://dummyimage.com/132x172.jpg/cc0000/ffffff	Baru	15	21594813884892270596
98	Hotwheels	Indigo	http://dummyimage.com/180x166.png/dddddd/000000	Kinclong	18	19948537697376309029
99	Baju	Purple	http://dummyimage.com/133x204.jpg/dddddd/000000	Baik	30	74024871534734781148
100	Digimon	Blue	http://dummyimage.com/127x158.bmp/cc0000/ffffff	Kinclong	19	39911274904286030651
3	Sasuke	Green	http://dummyimage.com/127x213.png/dddddd/000000	Sedang disewa anggota	13	98652913552510199993
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.barang_dikembalikan (no_resi, no_urut, id_barang) FROM stdin;
1	1	90
1	2	3
1	3	39
1	4	82
1	5	71
1	6	40
4	7	62
4	8	7
4	9	29
4	10	79
4	11	92
4	12	65
4	13	92
5	14	8
5	15	66
5	16	57
5	17	63
5	18	50
5	19	62
6	20	78
6	21	92
6	22	75
6	23	32
6	24	8
6	25	52
6	26	60
7	27	28
17	39	14
17	40	45
18	41	52
19	42	91
19	43	65
19	44	30
19	45	8
19	46	51
24	50	62
24	51	70
24	52	53
24	53	79
24	54	86
26	55	67
26	56	49
26	57	11
26	58	76
26	59	20
28	64	67
28	65	4
28	66	46
28	67	24
28	68	16
29	69	4
29	70	69
29	71	16
29	72	54
29	73	94
29	74	94
32	75	90
32	76	38
32	77	47
32	78	37
32	79	52
32	80	75
32	81	54
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
1	1	90	2019-01-02	Sesuai Gambar
1	2	3	2019-01-04	Ga sesuai gambar
1	3	39	2019-01-02	Aku merasa tertipu
1	4	82	2019-01-04	Sesuai Gambar
1	5	71	2019-01-02	Ga sesuai gambar
1	6	40	2019-01-02	Sesuai Gambar
4	7	62	2019-01-04	Aku merasa tertipu
4	8	7	2019-01-05	Ga sesuai gambar
4	9	29	2019-01-07	Bagus
4	10	79	2019-01-05	Ga sesuai gambar
4	11	92	2019-01-07	Bagus
4	12	65	2019-01-03	Sesuai Gambar
4	13	92	2019-01-06	Aku merasa tertipu
5	14	8	2019-01-04	Bagus
5	15	66	2019-01-04	Aku merasa tertipu
5	16	57	2019-01-06	Aku merasa tertipu
5	17	63	2019-01-04	Aku merasa tertipu
5	18	50	2019-01-05	Ga sesuai gambar
5	19	62	2019-01-06	Aku merasa tertipu
6	20	78	2019-01-05	Aku merasa tertipu
6	21	92	2019-01-07	Bagus
6	22	75	2019-01-09	Aku merasa tertipu
6	23	32	2019-01-05	Ga sesuai gambar
6	24	8	2019-01-06	Aku merasa tertipu
6	25	52	2019-01-06	Ga sesuai gambar
6	26	60	2019-01-07	Aku merasa tertipu
7	27	28	2019-01-08	Aku merasa tertipu
8	28	60	2019-01-07	Aku merasa tertipu
8	29	57	2019-01-10	Bagus
10	30	27	2019-01-09	Aku merasa tertipu
10	31	23	2019-01-10	Sesuai Gambar
10	32	13	2019-01-12	Sesuai Gambar
12	33	96	2019-01-11	Ga sesuai gambar
12	34	5	2019-01-12	Sesuai Gambar
14	35	90	2019-01-13	Ga sesuai gambar
15	36	64	2019-01-13	Sesuai Gambar
15	37	38	2019-01-11	Bagus
15	38	61	2019-01-14	Bagus
17	39	14	2019-01-15	Ga sesuai gambar
17	40	45	2019-01-14	Sesuai Gambar
18	41	52	2019-01-17	Ga sesuai gambar
19	42	91	2019-01-17	Aku merasa tertipu
19	43	65	2019-01-18	Bagus
19	44	30	2019-01-14	Aku merasa tertipu
19	45	8	2019-01-17	Ga sesuai gambar
19	46	51	2019-01-14	Bagus
22	47	56	2019-01-17	Sesuai Gambar
22	48	34	2019-01-17	Sesuai Gambar
22	49	54	2019-01-17	Aku merasa tertipu
24	50	62	2019-01-17	Ga sesuai gambar
24	51	70	2019-01-20	Sesuai Gambar
24	52	53	2019-01-16	Sesuai Gambar
24	53	79	2019-01-17	Bagus
24	54	86	2019-01-20	Aku merasa tertipu
26	55	67	2019-01-17	Ga sesuai gambar
26	56	49	2019-01-17	Ga sesuai gambar
26	57	11	2019-01-17	Bagus
26	58	76	2019-01-19	Ga sesuai gambar
26	59	20	2019-01-17	Ga sesuai gambar
27	60	10	2019-01-20	Bagus
27	61	94	2019-01-22	Bagus
27	62	32	2019-01-18	Ga sesuai gambar
27	63	100	2019-01-20	Bagus
28	64	67	2019-01-23	Bagus
28	65	4	2019-01-20	Bagus
28	66	46	2019-01-22	Aku merasa tertipu
28	67	24	2019-01-22	Bagus
28	68	16	2019-01-22	Bagus
29	69	4	2019-01-22	Aku merasa tertipu
29	70	69	2019-01-22	Sesuai Gambar
29	71	16	2019-01-22	Aku merasa tertipu
29	72	54	2019-01-22	Aku merasa tertipu
29	73	94	2019-01-20	Bagus
29	74	94	2019-01-22	Sesuai Gambar
32	75	90	2019-01-23	Bagus
32	76	38	2019-01-23	Sesuai Gambar
32	77	47	2019-01-25	Ga sesuai gambar
32	78	37	2019-01-25	Aku merasa tertipu
32	79	52	2019-01-24	Sesuai Gambar
32	80	75	2019-01-23	Aku merasa tertipu
32	81	54	2019-01-22	Bagus
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, nama_status) FROM stdin;
1	1	90	2019-01-01	1	2019-01-02	Sudah dikembalikan
1	2	3	2019-01-01	3	2019-01-04	Sudah dikembalikan
1	3	39	2019-01-01	1	2019-01-02	Sudah dikembalikan
1	4	82	2019-01-01	3	2019-01-04	Sudah dikembalikan
1	5	71	2019-01-01	1	2019-01-02	Sudah dikembalikan
1	6	40	2019-01-01	1	2019-01-02	Sudah dikembalikan
2	1	62	2019-01-02	2	2019-01-04	Sedang Dikonfirmasi
2	2	7	2019-01-02	3	2019-01-05	Sedang Dikonfirmasi
2	3	29	2019-01-02	5	2019-01-07	Sedang Dikonfirmasi
2	4	79	2019-01-02	3	2019-01-05	Sedang Dikonfirmasi
2	5	92	2019-01-02	5	2019-01-07	Sedang Dikonfirmasi
2	6	65	2019-01-02	1	2019-01-03	Sedang Dikonfirmasi
2	7	92	2019-01-02	4	2019-01-06	Sedang Dikonfirmasi
3	1	8	2019-01-03	1	2019-01-04	Menunggu Pembayaran
3	2	66	2019-01-03	1	2019-01-04	Menunggu Pembayaran
3	3	57	2019-01-03	3	2019-01-06	Menunggu Pembayaran
3	4	63	2019-01-03	1	2019-01-04	Menunggu Pembayaran
3	5	50	2019-01-03	2	2019-01-05	Menunggu Pembayaran
3	6	62	2019-01-03	3	2019-01-06	Menunggu Pembayaran
4	1	78	2019-01-04	1	2019-01-05	Sudah dikembalikan
4	2	92	2019-01-04	3	2019-01-07	Sudah dikembalikan
4	3	75	2019-01-04	5	2019-01-09	Sudah dikembalikan
4	4	32	2019-01-04	1	2019-01-05	Sudah dikembalikan
4	5	8	2019-01-04	2	2019-01-06	Sudah dikembalikan
4	6	52	2019-01-04	2	2019-01-06	Sudah dikembalikan
4	7	60	2019-01-04	3	2019-01-07	Sudah dikembalikan
5	1	28	2019-01-05	3	2019-01-08	Sudah dikembalikan
6	1	60	2019-01-06	1	2019-01-07	Sudah dikembalikan
6	2	57	2019-01-06	4	2019-01-10	Sudah dikembalikan
7	1	27	2019-01-07	2	2019-01-09	Sudah dikembalikan
7	2	23	2019-01-07	3	2019-01-10	Sudah dikembalikan
7	3	13	2019-01-07	5	2019-01-12	Sudah dikembalikan
8	1	96	2019-01-08	3	2019-01-11	Sudah dikembalikan
8	2	5	2019-01-08	4	2019-01-12	Sudah dikembalikan
9	1	90	2019-01-09	4	2019-01-13	Batal
10	1	64	2019-01-10	3	2019-01-13	Sudah dikembalikan
10	2	38	2019-01-10	1	2019-01-11	Sudah dikembalikan
10	3	61	2019-01-10	4	2019-01-14	Sudah dikembalikan
11	1	14	2019-01-11	4	2019-01-15	Menunggu Pembayaran
11	2	45	2019-01-11	3	2019-01-14	Menunggu Pembayaran
12	1	52	2019-01-12	5	2019-01-17	Sedang Dikirim
13	1	91	2019-01-13	4	2019-01-17	Sedang Disiapkan
13	2	65	2019-01-13	5	2019-01-18	Sedang Disiapkan
13	3	30	2019-01-13	1	2019-01-14	Sedang Disiapkan
13	4	8	2019-01-13	4	2019-01-17	Sedang Disiapkan
13	5	51	2019-01-13	1	2019-01-14	Sedang Disiapkan
14	1	56	2019-01-14	3	2019-01-17	Sedang Dikirim
14	2	34	2019-01-14	3	2019-01-17	Sedang Dikirim
14	3	54	2019-01-14	3	2019-01-17	Sedang Dikirim
15	1	62	2019-01-15	2	2019-01-17	Dalam Masa Sewa
15	2	70	2019-01-15	5	2019-01-20	Dalam Masa Sewa
15	3	53	2019-01-15	1	2019-01-16	Dalam Masa Sewa
15	4	79	2019-01-15	2	2019-01-17	Dalam Masa Sewa
15	5	86	2019-01-15	5	2019-01-20	Dalam Masa Sewa
16	1	67	2019-01-16	1	2019-01-17	Menunggu Pembayaran
16	2	49	2019-01-16	1	2019-01-17	Menunggu Pembayaran
16	3	11	2019-01-16	1	2019-01-17	Menunggu Pembayaran
16	4	76	2019-01-16	3	2019-01-19	Menunggu Pembayaran
16	5	20	2019-01-16	1	2019-01-17	Menunggu Pembayaran
17	1	10	2019-01-17	3	2019-01-20	Sudah dikembalikan
17	2	94	2019-01-17	5	2019-01-22	Sudah dikembalikan
17	3	32	2019-01-17	1	2019-01-18	Sudah dikembalikan
17	4	100	2019-01-17	3	2019-01-20	Sudah dikembalikan
18	1	67	2019-01-18	5	2019-01-23	Sudah dikembalikan
18	2	4	2019-01-18	2	2019-01-20	Sudah dikembalikan
18	3	46	2019-01-18	4	2019-01-22	Sudah dikembalikan
18	4	24	2019-01-18	4	2019-01-22	Sudah dikembalikan
18	5	16	2019-01-18	4	2019-01-22	Sudah dikembalikan
19	1	4	2019-01-19	3	2019-01-22	Sudah dikembalikan
19	2	69	2019-01-19	3	2019-01-22	Sudah dikembalikan
19	3	16	2019-01-19	3	2019-01-22	Sudah dikembalikan
19	4	54	2019-01-19	3	2019-01-22	Sudah dikembalikan
19	5	94	2019-01-19	1	2019-01-20	Sudah dikembalikan
19	6	94	2019-01-19	3	2019-01-22	Sudah dikembalikan
20	1	90	2019-01-20	3	2019-01-23	Batal
20	2	38	2019-01-20	3	2019-01-23	Batal
20	3	47	2019-01-20	5	2019-01-25	Batal
20	4	37	2019-01-20	5	2019-01-25	Batal
20	5	52	2019-01-20	4	2019-01-24	Batal
20	6	75	2019-01-20	3	2019-01-23	Batal
20	7	54	2019-01-20	2	2019-01-22	Batal
21	1	45	2019-01-21	3	2019-01-24	Sedang Dikonfirmasi
21	2	38	2019-01-21	3	2019-01-24	Sedang Dikonfirmasi
21	3	32	2019-01-21	1	2019-01-22	Sedang Dikonfirmasi
21	4	64	2019-01-21	4	2019-01-25	Sedang Dikonfirmasi
21	5	4	2019-01-21	1	2019-01-22	Sedang Dikonfirmasi
21	6	51	2019-01-21	1	2019-01-22	Sedang Dikonfirmasi
22	1	69	2019-01-22	5	2019-01-27	Dalam Masa Sewa
22	2	59	2019-01-22	4	2019-01-26	Dalam Masa Sewa
22	3	79	2019-01-22	2	2019-01-24	Dalam Masa Sewa
22	4	11	2019-01-22	2	2019-01-24	Dalam Masa Sewa
22	5	72	2019-01-22	4	2019-01-26	Dalam Masa Sewa
23	1	3	2019-01-23	4	2019-01-27	Batal
23	2	62	2019-01-23	4	2019-01-27	Batal
23	3	64	2019-01-23	1	2019-01-24	Batal
23	4	61	2019-01-23	4	2019-01-27	Batal
23	5	98	2019-01-23	4	2019-01-27	Batal
23	6	100	2019-01-23	3	2019-01-26	Batal
23	7	87	2019-01-23	1	2019-01-24	Batal
24	1	27	2019-01-24	1	2019-01-25	Sudah dikembalikan
24	2	43	2019-01-24	4	2019-01-28	Sudah dikembalikan
24	3	68	2019-01-24	5	2019-01-29	Sudah dikembalikan
24	4	92	2019-01-24	1	2019-01-25	Sudah dikembalikan
24	5	79	2019-01-24	4	2019-01-28	Sudah dikembalikan
24	6	42	2019-01-24	4	2019-01-28	Sudah dikembalikan
24	7	39	2019-01-24	4	2019-01-28	Sudah dikembalikan
25	1	6	2019-01-25	3	2019-01-28	Batal
25	2	82	2019-01-25	4	2019-01-29	Batal
25	3	51	2019-01-25	1	2019-01-26	Batal
25	4	22	2019-01-25	3	2019-01-28	Batal
25	5	17	2019-01-25	5	2019-01-30	Batal
25	6	91	2019-01-25	1	2019-01-26	Batal
26	1	58	2019-01-26	3	2019-01-29	Sudah dikembalikan
26	2	89	2019-01-26	4	2019-01-30	Sudah dikembalikan
26	3	72	2019-01-26	1	2019-01-27	Sudah dikembalikan
26	4	32	2019-01-26	5	2019-01-31	Sudah dikembalikan
26	5	53	2019-01-26	2	2019-01-28	Sudah dikembalikan
26	6	100	2019-01-26	2	2019-01-28	Sudah dikembalikan
27	1	74	2019-01-27	2	2019-01-29	Sudah dikembalikan
27	2	96	2019-01-27	2	2019-01-29	Sudah dikembalikan
27	3	52	2019-01-27	4	2019-01-31	Sudah dikembalikan
27	4	35	2019-01-27	2	2019-01-29	Sudah dikembalikan
27	5	4	2019-01-27	2	2019-01-29	Sudah dikembalikan
27	6	28	2019-01-27	5	2019-02-01	Sudah dikembalikan
27	7	47	2019-01-27	1	2019-01-28	Sudah dikembalikan
28	1	47	2019-01-28	2	2019-01-30	Sudah dikembalikan
28	2	40	2019-01-28	3	2019-01-31	Sudah dikembalikan
29	1	91	2019-01-29	3	2019-02-01	Sudah dikembalikan
29	2	6	2019-01-29	4	2019-02-02	Sudah dikembalikan
29	3	77	2019-01-29	4	2019-02-02	Sudah dikembalikan
29	4	85	2019-01-29	2	2019-01-31	Sudah dikembalikan
30	1	2	2019-01-30	4	2019-02-03	Menunggu Pembayaran
30	2	83	2019-01-30	2	2019-02-01	Menunggu Pembayaran
30	3	21	2019-01-30	1	2019-01-31	Menunggu Pembayaran
31	1	68	2019-01-31	3	2019-02-03	Sedang Disiapkan
31	2	32	2019-01-31	4	2019-02-04	Sedang Disiapkan
31	3	99	2019-01-31	2	2019-02-02	Sedang Disiapkan
31	4	98	2019-01-31	5	2019-02-05	Sedang Disiapkan
32	1	31	2019-02-01	2	2019-02-03	Sudah dikembalikan
32	2	76	2019-02-01	3	2019-02-04	Sudah dikembalikan
33	1	74	2019-02-02	4	2019-02-06	Sudah dikembalikan
33	2	89	2019-02-02	1	2019-02-03	Sudah dikembalikan
33	3	47	2019-02-02	2	2019-02-04	Sudah dikembalikan
33	4	22	2019-02-02	5	2019-02-07	Sudah dikembalikan
34	1	69	2019-02-03	4	2019-02-07	Sudah dikembalikan
34	2	92	2019-02-03	3	2019-02-06	Sudah dikembalikan
34	3	48	2019-02-03	3	2019-02-06	Sudah dikembalikan
34	4	89	2019-02-03	2	2019-02-05	Sudah dikembalikan
34	5	44	2019-02-03	5	2019-02-08	Sudah dikembalikan
34	6	50	2019-02-03	3	2019-02-06	Sudah dikembalikan
34	7	76	2019-02-03	3	2019-02-06	Sudah dikembalikan
35	1	55	2019-02-04	1	2019-02-05	Dalam Masa Sewa
35	2	58	2019-02-04	2	2019-02-06	Dalam Masa Sewa
36	1	98	2019-02-05	3	2019-02-08	Sudah dikembalikan
36	2	54	2019-02-05	1	2019-02-06	Sudah dikembalikan
36	3	19	2019-02-05	4	2019-02-09	Sudah dikembalikan
36	4	12	2019-02-05	2	2019-02-07	Sudah dikembalikan
36	5	83	2019-02-05	3	2019-02-08	Sudah dikembalikan
37	1	100	2019-02-06	4	2019-02-10	Batal
37	2	80	2019-02-06	1	2019-02-07	Batal
37	3	82	2019-02-06	2	2019-02-08	Batal
37	4	93	2019-02-06	3	2019-02-09	Batal
37	5	89	2019-02-06	5	2019-02-11	Batal
37	6	84	2019-02-06	2	2019-02-08	Batal
37	7	28	2019-02-06	5	2019-02-11	Batal
38	1	76	2019-02-07	3	2019-02-10	Sudah dikembalikan
38	2	89	2019-02-07	4	2019-02-11	Sudah dikembalikan
38	3	100	2019-02-07	1	2019-02-08	Sudah dikembalikan
38	4	71	2019-02-07	3	2019-02-10	Sudah dikembalikan
38	5	39	2019-02-07	2	2019-02-09	Sudah dikembalikan
38	6	89	2019-02-07	1	2019-02-08	Sudah dikembalikan
39	1	90	2019-02-08	1	2019-02-09	Sudah dikembalikan
40	1	38	2019-02-09	1	2019-02-10	Menunggu Pembayaran
40	2	75	2019-02-09	2	2019-02-11	Menunggu Pembayaran
40	3	21	2019-02-09	2	2019-02-11	Menunggu Pembayaran
41	1	32	2019-02-10	4	2019-02-14	Sedang Disiapkan
41	2	39	2019-02-10	4	2019-02-14	Sedang Disiapkan
41	3	97	2019-02-10	1	2019-02-11	Sedang Disiapkan
41	4	90	2019-02-10	3	2019-02-13	Sedang Disiapkan
42	1	48	2019-02-11	1	2019-02-12	Sedang Dikirim
42	2	87	2019-02-11	5	2019-02-16	Sedang Dikirim
43	1	9	2019-02-12	4	2019-02-16	Sudah dikembalikan
43	2	38	2019-02-12	3	2019-02-15	Sudah dikembalikan
44	1	64	2019-02-13	3	2019-02-16	Sudah dikembalikan
44	2	16	2019-02-13	4	2019-02-17	Sudah dikembalikan
44	3	32	2019-02-13	2	2019-02-15	Sudah dikembalikan
44	4	28	2019-02-13	5	2019-02-18	Sudah dikembalikan
45	1	3	2019-02-14	4	2019-02-18	Batal
46	1	70	2019-02-15	2	2019-02-17	Sedang Dikirim
46	2	73	2019-02-15	4	2019-02-19	Sedang Dikirim
46	3	82	2019-02-15	3	2019-02-18	Sedang Dikirim
46	4	97	2019-02-15	2	2019-02-17	Sedang Dikirim
46	5	65	2019-02-15	5	2019-02-20	Sedang Dikirim
47	1	54	2019-02-16	3	2019-02-19	Batal
47	2	7	2019-02-16	4	2019-02-20	Batal
47	3	42	2019-02-16	4	2019-02-20	Batal
47	4	66	2019-02-16	5	2019-02-21	Batal
47	5	86	2019-02-16	1	2019-02-17	Batal
47	6	54	2019-02-16	1	2019-02-17	Batal
48	1	71	2019-02-17	4	2019-02-21	Menunggu Pembayaran
48	2	94	2019-02-17	3	2019-02-20	Menunggu Pembayaran
48	3	40	2019-02-17	1	2019-02-18	Menunggu Pembayaran
48	4	19	2019-02-17	2	2019-02-19	Menunggu Pembayaran
48	5	19	2019-02-17	4	2019-02-21	Menunggu Pembayaran
49	1	81	2019-02-18	4	2019-02-22	Sudah dikembalikan
50	1	92	2019-02-19	5	2019-02-24	Sudah dikembalikan
50	2	37	2019-02-19	4	2019-02-23	Sudah dikembalikan
5	2	3	2019-01-01	1	2019-01-03	Sedang Dikonfirmasi
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.chat (id, pesan, date_time, no_ktp_anggota, no_ktp_admin) FROM stdin;
mznr1kymjcymd4b	sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis	2019-02-14 18:23:23	39911274904286030651	73315370623766663997
nvn4tgrtoba1cht	nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum	2018-11-03 18:53:13	63562589239122852947	99209299096497566156
q3hr6fkmzrgtvtm	potenti in eleifend quam a odio in hac habitasse platea dictumst	2019-01-23 06:53:17	14165759167360865502	24032874520349018291
5k0q16620dsozwc	leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu	2019-03-08 09:16:21	60171449186896735313	95155846837355220109
mzoii1w4n3nemn5	urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam	2018-08-30 14:20:51	38004888765418454368	08301252739722567087
cdu8foqwj14736l	volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est	2019-04-04 13:26:10	34021506316616141151	50035056544919604759
u9i10af3ft8jsnq	non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla	2018-12-03 20:59:17	72905554931015048977	85871230537203328052
e87ggfcvso0xb63	tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus	2018-04-11 12:41:56	54368352265519664000	03776350565147233050
38dc7o9slkswbry	consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et	2018-06-30 08:51:09	20948543328395218639	08301252739722567087
3ozwimpo2ifdpkn	mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus	2019-04-10 12:22:35	74024871534734781148	64240430504127626817
dyuozdvaqrcnces	leo maecenas pulvinar lobortis est phasellus sit amet erat nulla	2019-01-01 05:11:44	66315564854348311821	85871230537203328052
fiilwsvyk4cyhiw	arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium	2018-12-19 19:22:24	07639382540165082999	73315370623766663997
4gclvwsiciy306h	duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus	2018-04-20 15:05:15	42140288875353525504	24032874520349018291
jei8i36igfbtt2i	justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet	2018-05-20 04:21:04	73672218364347144165	95155846837355220109
odhj8vlg4q6gnb9	commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus	2018-09-12 00:48:06	72020110548056782343	24032874520349018291
798halhesjb7com	enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue	2019-01-21 22:58:55	40943312463493083803	50035056544919604759
v1ayndeci2vqxif	massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat	2018-11-20 15:08:09	28446781511455104547	85871230537203328052
c0iiq672iglh6v3	nunc commodo placerat praesent blandit nam nulla integer pede justo	2018-10-17 02:56:06	73249788789082064681	50035056544919604759
d5ug3cu9utp4jha	lorem ipsum dolor sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut	2018-06-03 01:33:42	09326281851165173221	50035056544919604759
wu3p5a8v1uxer1q	id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras	2018-10-27 19:09:45	63510126440951680126	99209299096497566156
n4azjif1f763z40	ullamcorper augue a suscipit nulla elit ac nulla sed vel	2018-07-04 20:39:23	92765419216039307203	85871230537203328052
1pwewj0u0kvyfbx	dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id	2018-07-20 17:37:23	39911274904286030651	14819673651963931895
r85h7vu3dwe93z4	adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis	2019-02-18 07:06:10	74024871534734781148	24032874520349018291
ijygm8b9fjb15uf	ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae	2019-02-19 00:54:47	21594813884892270596	08301252739722567087
6i4v0zc5i3il91r	mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet	2018-08-19 17:53:35	28446781511455104547	73315370623766663997
l83ifyztqmjhzfp	lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis	2018-06-22 20:56:34	97009377697291677089	95155846837355220109
0z1g1ok40ldjk34	nam congue risus semper porta volutpat quam pede lobortis ligula sit	2018-11-21 00:00:03	78014214586158244148	08301252739722567087
iqh6q3nx2hwmuuy	leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit	2018-12-30 23:04:03	16312505491989204658	14819673651963931895
204849jjsx8qd2e	massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien	2018-06-19 03:09:38	78014214586158244148	85871230537203328052
zda1iuj1qeze7lo	cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus	2018-10-10 06:06:09	33865363860970895904	50035056544919604759
ujxaiw6bhq5373i	adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede	2019-02-21 18:18:58	72020110548056782343	14819673651963931895
c587sjf5oov5zfj	bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis	2018-11-19 09:54:19	72412300250595634568	24032874520349018291
nmtfd9bxuarhj47	vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget	2018-05-17 16:58:43	40243728783507793290	14819673651963931895
fe7xtqsp6ioqx0k	vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et	2018-05-03 01:45:07	98173357558824190885	14819673651963931895
5lx59lo99dn0uzb	praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum	2018-11-24 05:39:55	93741652064609229097	50035056544919604759
41epknknnhwg5ow	nisi at nibh in hac habitasse platea dictumst aliquam augue quam	2018-04-29 00:37:15	98652913552510199993	95155846837355220109
6nfmomwd0u2oef8	posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a	2018-07-29 04:45:22	83170353179132776193	08301252739722567087
y5sjjh5dp0drcgv	vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia	2019-02-08 01:05:05	36840789280324764449	03776350565147233050
fkwoz8jmh79xjof	feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa	2019-01-13 22:11:30	40243728783507793290	85871230537203328052
sr3j2caqzxt0dqv	ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem	2018-05-13 14:49:09	02042048624558529710	50035056544919604759
4mvvod48zlrs2vr	metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et	2018-07-29 21:48:39	92765419216039307203	99209299096497566156
ve8ysqxqswqs7vt	nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue	2018-12-28 04:46:40	60820969271826157051	08301252739722567087
r010do30rpblamb	erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec	2018-05-16 17:50:32	53196963634942900541	24032874520349018291
27npc5vgnk45iv4	semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac	2018-05-21 13:04:01	89031875319756622272	95155846837355220109
7egstrs44a158hc	orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut	2019-04-06 00:37:16	21594813884892270596	14819673651963931895
yv8fjicvdk1evht	pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla	2018-12-19 05:16:41	74024871534734781148	73315370623766663997
y8a7ggnebxvsjny	dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis	2018-07-09 03:44:38	42433278063120933230	14819673651963931895
81hnfnme06430b4	cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae	2018-11-19 20:04:01	73249788789082064681	99209299096497566156
dd68p6vmltjdu0w	sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean	2018-12-15 07:07:42	10160532657929709480	99209299096497566156
toewda1x91w91b9	pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing	2018-05-25 12:22:24	20948543328395218639	95155846837355220109
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
1	Bronze	919150.75	0.150000006
2	Bronze	862743.188	0.159999996
3	Bronze	322863.312	0.00999999978
4	Bronze	867627	0.230000004
5	Bronze	625662.688	0.180000007
6	Bronze	650052.312	0.129999995
7	Bronze	990076.688	0.200000003
8	Bronze	539003.875	0.170000002
9	Bronze	513324.469	0.200000003
10	Bronze	917053.75	0.159999996
11	Bronze	491118.094	0.0900000036
12	Bronze	314689.781	0.100000001
13	Bronze	868949.375	0.0799999982
14	Bronze	571157.5	0.0500000007
15	Bronze	585497.812	0.109999999
16	Bronze	809057.25	0.170000002
17	Bronze	802840.5	0.0399999991
18	Bronze	598798.625	0.289999992
19	Bronze	533224.75	0.180000007
20	Bronze	869961.688	0.209999993
21	Bronze	393511.5	0.25999999
22	Bronze	542856.438	0.25999999
23	Bronze	419006.875	0.150000006
24	Bronze	912241	0.129999995
25	Bronze	833315.562	0.270000011
26	Bronze	613594	0.209999993
27	Bronze	851874.125	0.239999995
28	Bronze	308365.125	0.25999999
29	Bronze	378865.812	0.0299999993
30	Bronze	326577.219	0.0199999996
31	Bronze	714858.312	0.129999995
32	Bronze	421090.281	0.0299999993
33	Bronze	739648	0.180000007
34	Bronze	353797.5	0.200000003
35	Bronze	574795.688	0
36	Bronze	842169.438	0.159999996
37	Bronze	731086.188	0.0900000036
38	Bronze	792841.5	0.0700000003
39	Bronze	917998.25	0.0700000003
40	Bronze	464934.406	0.0500000007
41	Bronze	884843.062	0.0199999996
42	Bronze	311372.125	0.280000001
43	Bronze	549457	0.270000011
44	Bronze	523585.625	0.230000004
45	Bronze	706768.875	0.0700000003
46	Bronze	900720.75	0.219999999
47	Bronze	407274	0.280000001
48	Bronze	693014.562	0.25999999
49	Bronze	354883.312	0.0199999996
50	Bronze	823657.375	0.159999996
51	Bronze	850315.312	0.159999996
52	Bronze	894804.75	0.25
53	Bronze	349982.781	0.25
54	Bronze	426942.531	0.219999999
55	Bronze	697498	0.270000011
56	Bronze	527084.5	0.150000006
57	Bronze	522583.156	0.0199999996
58	Bronze	888342.938	0.140000001
59	Bronze	852462.562	0.170000002
60	Bronze	344358	0.140000001
61	Bronze	926898.688	0.209999993
62	Bronze	411779.25	0.129999995
63	Bronze	478573.75	0.100000001
64	Bronze	736133.5	0.189999998
65	Bronze	640126	0.0500000007
66	Bronze	958043.938	0.180000007
67	Bronze	966903	0.119999997
68	Bronze	354667.938	0.239999995
69	Bronze	963211.188	0.0500000007
70	Bronze	409738.75	0.159999996
71	Bronze	413969.938	0.119999997
72	Bronze	425291.062	0.239999995
73	Bronze	571267	0.0399999991
74	Bronze	637731.438	0.0500000007
75	Bronze	416345.188	0.209999993
76	Bronze	623454.562	0.239999995
77	Bronze	436145.906	0.300000012
78	Bronze	965417.125	0.159999996
79	Bronze	495325.656	0.0700000003
80	Bronze	498454.875	0.189999998
81	Bronze	371225.344	0.0900000036
82	Bronze	480018.875	0.150000006
83	Bronze	766862.938	0.150000006
84	Bronze	720395.812	0.0500000007
85	Bronze	427164.344	0.0399999991
86	Bronze	428400	0.280000001
87	Bronze	878626.438	0.0399999991
88	Bronze	706747.375	0.289999992
89	Bronze	778080.562	0.0799999982
90	Bronze	856135.438	0.170000002
91	Bronze	554203.812	0.289999992
92	Bronze	534635.812	0.230000004
93	Bronze	593419.062	0.25
94	Bronze	623203.5	0.0399999991
95	Bronze	569823.5	0.0500000007
96	Bronze	802818	0.140000001
97	Bronze	703100.438	0.289999992
98	Bronze	721213	0.150000006
99	Bronze	489262.781	0.230000004
100	Bronze	834137.062	0.100000001
1	Gold	77768.5	0.74000001
2	Gold	13261	0.930000007
3	Gold	34866.2383	0.660000026
4	Gold	22713.3203	0.74000001
5	Gold	17036.1504	0.620000005
6	Gold	42539.8516	0.889999986
7	Gold	56198.6914	0.680000007
8	Gold	68981.1328	1
9	Gold	65336.0508	0.699999988
10	Gold	49093.3203	0.980000019
11	Gold	70946.3438	0.829999983
12	Gold	51393.3398	0.629999995
13	Gold	14323.3301	0.75999999
14	Gold	96597.7578	0.620000005
15	Gold	79746.9766	0.779999971
16	Gold	41430.9414	0.939999998
17	Gold	58335.0898	0.839999974
18	Gold	17866	0.779999971
19	Gold	91190.7891	0.660000026
20	Gold	96579.0469	0.800000012
21	Gold	82504.7734	0.839999974
22	Gold	80134.9297	0.980000019
23	Gold	45311.0781	0.930000007
24	Gold	57732.4805	0.689999998
25	Gold	31385.2402	0.970000029
26	Gold	77361.7422	0.860000014
27	Gold	20096.75	0.910000026
28	Gold	95422.2422	0.920000017
29	Gold	98864.9531	0.959999979
30	Gold	82245.1797	0.959999979
31	Gold	21885.4902	0.879999995
32	Gold	22384.0098	0.930000007
33	Gold	67810.5	0.75
34	Gold	20494.0996	0.620000005
35	Gold	45502.1406	0.720000029
36	Gold	96623.0312	0.75999999
37	Gold	84878.0938	0.75999999
38	Gold	53581.8008	0.620000005
39	Gold	97205.3281	0.610000014
40	Gold	95080.0391	0.850000024
41	Gold	14001.1904	0.600000024
42	Gold	98340.0391	0.629999995
43	Gold	20000.0996	0.860000014
44	Gold	71856.4297	0.889999986
45	Gold	31178.9395	0.970000029
46	Gold	23872.6094	0.629999995
47	Gold	49798.3203	0.980000019
48	Gold	80059.2812	0.649999976
49	Gold	10284.5195	0.970000029
50	Gold	78775.5781	0.790000021
51	Gold	67107.5078	0.860000014
52	Gold	48117.7305	0.870000005
53	Gold	54654.8281	0.879999995
54	Gold	30412.0195	0.629999995
55	Gold	57703.3516	0.610000014
56	Gold	42465.4688	0.819999993
57	Gold	89614.5625	0.920000017
58	Gold	84356.5391	1
59	Gold	41245.6211	0.600000024
60	Gold	53786.7891	0.810000002
61	Gold	61536.1797	0.980000019
62	Gold	58170.7617	0.899999976
63	Gold	83040.7031	0.899999976
64	Gold	87294.5469	0.649999976
65	Gold	13991.9502	0.959999979
66	Gold	81926.0312	0.779999971
67	Gold	78376.8984	0.879999995
68	Gold	17160.8105	0.800000012
69	Gold	84408.9062	0.769999981
70	Gold	72626.6875	0.720000029
71	Gold	30666	0.75
72	Gold	83820.3438	0.720000029
73	Gold	86433.7891	0.600000024
74	Gold	13264.3799	0.639999986
75	Gold	21039.9102	1
76	Gold	88689.5234	0.949999988
77	Gold	49377.3984	0.610000014
78	Gold	90643.7578	0.810000002
79	Gold	99466.6797	0.910000026
80	Gold	63526.0898	0.720000029
81	Gold	63057.6797	0.610000014
82	Gold	43387.5508	0.819999993
83	Gold	72865.0469	0.829999983
84	Gold	64912.2617	0.600000024
85	Gold	67499.9766	0.769999981
86	Gold	30382.1992	0.870000005
87	Gold	85428.8906	0.779999971
88	Gold	59590.3789	0.620000005
89	Gold	50530.9414	1
90	Gold	15611.3896	0.74000001
91	Gold	96919.2812	0.680000007
92	Gold	73041.8125	0.629999995
93	Gold	98737.9219	0.800000012
94	Gold	85671.0938	0.860000014
95	Gold	74592.1719	0.680000007
96	Gold	17098.1309	0.699999988
97	Gold	62044.4219	0.649999976
98	Gold	45131.4414	0.720000029
99	Gold	34003.4219	0.709999979
100	Gold	11609.04	0.709999979
1	Silver	102086.523	0.360000014
2	Silver	299361.844	0.529999971
3	Silver	123734.391	0.479999989
4	Silver	122119.977	0.379999995
5	Silver	282515.875	0.400000006
6	Silver	142838.516	0.310000002
7	Silver	206414.656	0.569999993
8	Silver	297681.281	0.430000007
9	Silver	146224.094	0.49000001
10	Silver	203017	0.310000002
11	Silver	204759.891	0.479999989
12	Silver	295782.5	0.5
13	Silver	262572.188	0.50999999
14	Silver	292313.344	0.419999987
15	Silver	210537.953	0.460000008
16	Silver	170285.016	0.560000002
17	Silver	201807.375	0.400000006
18	Silver	177707.844	0.449999988
19	Silver	274121.688	0.409999996
20	Silver	250218.766	0.389999986
21	Silver	153357.594	0.400000006
22	Silver	182705.797	0.310000002
23	Silver	223222.953	0.360000014
24	Silver	260059.266	0.439999998
25	Silver	133597.203	0.360000014
26	Silver	272372.75	0.300000012
27	Silver	103189.562	0.469999999
28	Silver	130564.789	0.419999987
29	Silver	121559.898	0.319999993
30	Silver	140020.234	0.529999971
31	Silver	219013.062	0.419999987
32	Silver	275363.25	0.5
33	Silver	118978.18	0.560000002
34	Silver	265744.344	0.589999974
35	Silver	265761.812	0.519999981
36	Silver	120112.109	0.340000004
37	Silver	173218.719	0.409999996
38	Silver	187721.609	0.310000002
39	Silver	227225.312	0.50999999
40	Silver	182513.984	0.349999994
41	Silver	215644.344	0.419999987
42	Silver	215895.109	0.300000012
43	Silver	119121.641	0.400000006
44	Silver	102424.633	0.430000007
45	Silver	165003.141	0.569999993
46	Silver	205198.938	0.5
47	Silver	290151.438	0.370000005
48	Silver	100266.648	0.360000014
49	Silver	120542.328	0.409999996
50	Silver	199813.797	0.430000007
51	Silver	182770.766	0.310000002
52	Silver	190691.453	0.449999988
53	Silver	195943.844	0.439999998
54	Silver	169622.938	0.379999995
55	Silver	270691.25	0.370000005
56	Silver	272462.469	0.340000004
57	Silver	187876.438	0.379999995
58	Silver	211775.797	0.370000005
59	Silver	176934.781	0.319999993
60	Silver	229357.078	0.340000004
61	Silver	146065.25	0.439999998
62	Silver	263525.156	0.370000005
63	Silver	255868.688	0.540000021
64	Silver	212531.688	0.529999971
65	Silver	122203.711	0.439999998
66	Silver	173400.875	0.519999981
67	Silver	248928.156	0.389999986
68	Silver	146231.375	0.540000021
69	Silver	145633.688	0.529999971
70	Silver	109709.141	0.469999999
71	Silver	201658.891	0.439999998
72	Silver	165655.953	0.460000008
73	Silver	287576	0.379999995
74	Silver	277873.312	0.589999974
75	Silver	262594.406	0.439999998
76	Silver	158554.391	0.439999998
77	Silver	196225.344	0.540000021
78	Silver	150618.344	0.340000004
79	Silver	117655.219	0.300000012
80	Silver	162072.438	0.519999981
81	Silver	173746.656	0.430000007
82	Silver	203246.391	0.400000006
83	Silver	136694.547	0.449999988
84	Silver	214934.047	0.569999993
85	Silver	296928.906	0.379999995
86	Silver	195892.906	0.340000004
87	Silver	288086.031	0.370000005
88	Silver	247895.891	0.479999989
89	Silver	173142.078	0.439999998
90	Silver	298676.75	0.319999993
91	Silver	263935.188	0.49000001
92	Silver	230744	0.430000007
93	Silver	192188.875	0.439999998
94	Silver	283746.25	0.349999994
95	Silver	234979.953	0.550000012
96	Silver	295613.094	0.360000014
97	Silver	156003.797	0.419999987
98	Silver	240639.547	0.330000013
99	Silver	146204.484	0.370000005
100	Silver	180930.094	0.439999998
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
Piano	Bisa dipencet	1	10	\N
Gitar	Bisa digenjreng	4	13	Kayu
Bass	\N	2	8	\N
Chucky	Horor	1	3	\N
Rumah-rumahan	\N	3	15	Plastik
Ayunan	Bisa diayun	5	10	\N
Jigsaw Kecil	Ukuran Kecil	1	3	Kertas
Jigsaw Sedang	Ukuran Sedang	3	5	Kertas
Jigsaw Besar	Ukuran Besar	5	7	Kertas
Drum	Ukuran Kecil	4	10	\N
Plastisin	\N	2	8	Lilin
Origami	Mainan lipat	5	15	Kertas
Lego	\N	3	10	Plastik
Biola	Bisa digesek	10	12	\N
Suling	Bisa ditiup	5	7	\N
Rubik 2x2	Merek dari china	3	10	Plastik
Rubik 3x3	Merek dari china	4	10	Plastik
Rubik 4x4	Merek dari china	5	10	Plastik
Rubik Pyraminx	Merek dari china	6	10	Plastik
Nesoberi	Asli dari jepang	2	10	Kapas
Spongebob	\N	2	8	\N
Naruto	\N	5	15	PVC
Hatsune Miku	\N	5	12	\N
Asuna	Waifu sejuta umat	7	15	\N
Sasuke	Lawannya Naruto	5	15	\N
Kaos Kaki	Biar jago programming	0	10	Katun
Pokemon	\N	1	10	Kapas
Digimon	\N	1	10	Kapas
Baju	Bisa dipake	0	10	Katun
Bando	Dipake di kepala	3	7	\N
Celana	Dipake di kaki	3	7	Katun
Mahkota	Mirip bando	3	7	Plastik
Hotwheels	Mainan mobil	5	10	\N
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.kategori (nama, level, sub_dari) FROM stdin;
Boneka	1	\N
Musik	1	\N
Puzzle	1	\N
Rubik	2	Puzzle
Figma	2	Boneka
Action Figure	3	Figma
Jigsaw	2	Puzzle
Motorik	1	\N
Motorik Kasar	2	Motorik
Motorik Halus	2	Motorik
Plush	2	Boneka
Aksesoris Bayi	1	\N
Pakaian	2	Aksesoris Bayi
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.kategori_item (nama_item, nama_kategori) FROM stdin;
Piano	Musik
Gitar	Musik
Bass	Musik
Chucky	Boneka
Rumah-rumahan	Motorik
Ayunan	Motorik
Jigsaw Kecil	Puzzle
Jigsaw Sedang	Puzzle
Jigsaw Besar	Puzzle
Drum	Musik
Plastisin	Motorik
Origami	Motorik
Lego	Motorik
Biola	Musik
Suling	Musik
Rubik 2x2	Puzzle
Rubik 3x3	Puzzle
Rubik 4x4	Puzzle
Rubik Pyraminx	Puzzle
Nesoberi	Boneka
Spongebob	Boneka
Naruto	Figma
Hatsune Miku	Action Figure
Asuna	Action Figure
Sasuke	Figma
Rumah-rumahan	Motorik Kasar
Ayunan	Motorik Kasar
Jigsaw Kecil	Jigsaw
Jigsaw Sedang	Jigsaw
Jigsaw Besar	Jigsaw
Plastisin	Motorik Halus
Origami	Motorik Halus
Lego	Motorik Halus
Rubik 2x2	Rubik
Rubik 3x3	Rubik
Rubik 4x4	Rubik
Rubik Pyraminx	Rubik
Hatsune Miku	Motorik
Asuna	Motorik
Rumah-rumahan	Puzzle
Nesoberi	Motorik Halus
Spongebob	Motorik Halus
Piano	Motorik
Gitar	Motorik
Bass	Motorik
Drum	Motorik
Biola	Motorik
Suling	Motorik
Chucky	Motorik Halus
Nesoberi	Plush
Spongebob	Plush
Lego	Puzzle
Kaos Kaki	Aksesoris Bayi
Ayunan	Aksesoris Bayi
Pokemon	Plush
Digimon	Plush
Chucky	Action Figure
Baju	Aksesoris Bayi
Bando	Aksesoris Bayi
Rubik 2x2	Motorik
Rubik 3x3	Motorik
Rubik 4x4	Motorik
Rubik Pyraminx	Motorik
Celana	Aksesoris Bayi
Mahkota	Aksesoris Bayi
Mahkota	Motorik
Mahkota	Puzzle
Baju	Pakaian
Celana	Pakaian
Kaos Kaki	Pakaian
Gitar	Puzzle
Bass	Motorik Halus
Suling	Puzzle
Hotwheels	Motorik
Hotwheels	Puzzle
Hotwheels	Motorik Kasar
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
Bronze	0	Untuk Keanggotaan Baru
Silver	50	Untuk Keanggotaan Biasa
Gold	80	Untuk Keanggotaan Lama
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
1	2019-01-01 00:00:00	6	1237055.75	96036.9297	98805058936524648004	Sudah dikembalikan
2	2019-01-02 01:00:00	7	1292846.62	50814.3984	34021506316616141151	Sedang Dikonfirmasi
3	2019-01-03 09:00:00	6	3733641.25	78498.2031	88500163593013419402	Menunggu Pembayaran
4	2019-01-04 20:00:00	7	4115655	86289.9688	15132931449404851787	Sudah dikembalikan
6	2019-01-06 01:00:00	2	866941.188	52541.2812	67409469193068326386	Sudah dikembalikan
7	2019-01-07 15:00:00	3	2139830.5	48743.9414	63510126440951680126	Sudah dikembalikan
8	2019-01-08 15:00:00	2	578129	42833.5117	34021506316616141151	Sudah dikembalikan
9	2019-01-09 20:00:00	1	298676.75	86080.6719	99807022136199706322	Batal
10	2019-01-10 16:00:00	3	2455873.75	85277.5234	09643404999667107947	Sudah dikembalikan
11	2019-01-11 06:00:00	2	457316.5	19772.0703	74024871534734781148	Menunggu Pembayaran
12	2019-01-12 17:00:00	1	894804.75	65932.5312	20948543328395218639	Sedang Dikirim
13	2019-01-13 04:00:00	5	1006611.19	22293.4297	72905554931015048977	Sedang Disiapkan
14	2019-01-14 21:00:00	3	1307824.5	93651	65239212634009723486	Sedang Dikirim
15	2019-01-15 12:00:00	5	882726.25	21540.3594	34021506316616141151	Dalam Masa Sewa
16	2019-01-16 03:00:00	5	3306320.75	26769.3105	42140288875353525504	Menunggu Pembayaran
17	2019-01-17 02:00:00	4	2795484.5	70818.2969	68528283622701707948	Sudah dikembalikan
18	2019-01-18 07:00:00	5	4456549	13101.5	19948537697376309029	Sudah dikembalikan
19	2019-01-19 21:00:00	6	1175154.12	15668.21	40243728783507793290	Sudah dikembalikan
20	2019-01-20 14:00:00	7	1572677.38	22787.3496	74024871534734781148	Batal
21	2019-01-21 21:00:00	6	4374776.5	66147.7422	19948537697376309029	Sedang Dikonfirmasi
22	2019-01-22 04:00:00	5	810639.562	83303.75	39911274904286030651	Dalam Masa Sewa
23	2019-01-23 17:00:00	7	4831651.5	33620.1484	86203763546087351236	Batal
24	2019-01-24 18:00:00	7	1160062.25	35098.2305	98805058936524648004	Sudah dikembalikan
25	2019-01-25 20:00:00	6	1177304	38336.6992	10160532657929709480	Batal
26	2019-01-26 07:00:00	6	1202811	69846.5625	73605563756248532873	Sudah dikembalikan
27	2019-01-27 01:00:00	7	4493416	56402.5117	37035872630850054065	Sudah dikembalikan
28	2019-01-28 03:00:00	2	472665.438	51536.4297	98805058936524648004	Sudah dikembalikan
29	2019-01-29 10:00:00	4	899927.938	20114.7402	36840789280324764449	Sudah dikembalikan
30	2019-01-30 01:00:00	3	168630.812	70282.3828	43839378479279291640	Menunggu Pembayaran
31	2019-01-31 19:00:00	4	1986234	97052.2031	65239212634009723486	Sedang Disiapkan
32	2019-02-01 04:00:00	2	377567.469	41154.0781	10160532657929709480	Sudah dikembalikan
33	2019-02-02 05:00:00	4	923872.625	77811.7422	40243728783507793290	Sudah dikembalikan
34	2019-02-03 20:00:00	7	4939639.5	96440.8203	08325231543252789834	Sudah dikembalikan
35	2019-02-04 19:00:00	2	1585840.88	19350.9707	79637781754587917528	Dalam Masa Sewa
36	2019-02-05 06:00:00	5	1116861.25	89936.9609	73672218364347144165	Sudah dikembalikan
37	2019-02-06 15:00:00	7	1257078.75	25912.25	73672218364347144165	Batal
38	2019-02-07 00:00:00	6	1114652.88	27270.7402	78014214586158244148	Sudah dikembalikan
39	2019-02-08 22:00:00	1	856135.438	83184.5469	73734061093203616225	Sudah dikembalikan
40	2019-02-09 17:00:00	3	603673.625	89794.8594	87147487624650243570	Menunggu Pembayaran
41	2019-02-10 14:00:00	4	2898324.25	95041.2031	42110701225884818282	Sedang Disiapkan
42	2019-02-11 23:00:00	2	388352.656	20994.1992	58123227991230660569	Sedang Dikirim
43	2019-02-12 07:00:00	2	118917.852	69958.3125	35402646113236398928	Sudah dikembalikan
44	2019-02-13 08:00:00	4	2274646.25	20346.3008	86203763546087351236	Sudah dikembalikan
45	2019-02-14 16:00:00	1	123734.391	24743.1699	16312505491989204658	Batal
46	2019-02-15 12:00:00	5	2804251	53364.4492	09643404999667107947	Sedang Dikirim
47	2019-02-16 16:00:00	6	3541777.75	10563.0098	21255518690756411396	Batal
48	2019-02-17 13:00:00	5	2568557.25	17622.1602	28446781511455104547	Menunggu Pembayaran
49	2019-02-18 17:00:00	1	371225.344	79341.75	20948543328395218639	Sudah dikembalikan
50	2019-02-19 00:00:00	2	1265722	47566.5586	73506622399156528946	Sudah dikembalikan
5	2019-01-05 10:00:00	1	254299	21172.8008	66315564854348311821	Sudah dikembalikan
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
1	1	Jemput	14518.4297	2019-01-01	98805058936524648004	Rumah
2	4	antar	13114.9902	2019-01-04	15132931449404851787	Rumah
3	5	antar	39748.8594	2019-01-05	66315564854348311821	Rumah
4	6	antar	40172.1602	2019-01-06	67409469193068326386	Kantor
5	7	antar	24044.4492	2019-01-07	63510126440951680126	Kantor
6	8	antar	10647.2695	2019-01-08	34021506316616141151	Kantor
7	10	Jemput	38336.3008	2019-01-10	09643404999667107947	Kantor
11	17	Jemput	21241.1797	2019-01-17	68528283622701707948	Kantor
12	18	Jemput	33977.3594	2019-01-18	19948537697376309029	Rumah
13	19	antar	13814.3203	2019-01-19	40243728783507793290	Rumah
15	24	antar	15605.4697	2019-01-24	98805058936524648004	Rumah
16	26	Jemput	37091.3789	2019-01-26	73605563756248532873	Kantor
17	27	antar	38313.9414	2019-01-27	37035872630850054065	Kantor
18	28	antar	4022.78003	2019-01-28	98805058936524648004	Rumah
19	29	Jemput	10683.3398	2019-01-29	36840789280324764449	Kantor
20	32	antar	23857.3594	2019-02-01	10160532657929709480	Kantor
21	33	antar	36198.2188	2019-02-02	40243728783507793290	Rumah
22	34	antar	36641.9297	2019-02-03	08325231543252789834	Kantor
24	36	Jemput	17461.6895	2019-02-05	73672218364347144165	Kantor
25	38	Jemput	8993.46973	2019-02-07	78014214586158244148	Rumah
26	39	Jemput	34469.9219	2019-02-08	73734061093203616225	Rumah
28	43	Jemput	36504.4297	2019-02-12	35402646113236398928	Rumah
29	44	antar	35610.2891	2019-02-13	86203763546087351236	Rumah
31	49	antar	25648.4902	2019-02-18	20948543328395218639	Rumah
32	50	antar	18679.6504	2019-02-19	73506622399156528946	Rumah
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
54368352265519664000	Moina Slessor	mslessor0@weibo.com	1983-09-02	088188283769
69406617935543459583	Bertie De Bischop	bde1@wix.com	1989-08-03	085796174440
02042048624558529710	Morey Pyvis	mpyvis2@geocities.jp	1995-02-23	\N
34021506316616141151	Truman Pine	tpine3@cloudflare.com	1991-08-21	080830825843
53196963634942900541	Ulberto Teek	uteek4@howstuffworks.com	1981-07-01	081630431369
03829953029916687911	Trula Turbayne	tturbayne5@amazon.de	1989-05-12	086329505978
08301252739722567087	Winne Bumford	wbumford6@weibo.com	1982-10-07	084851080314
81686336623645601283	Dennis Aupol	daupol7@hud.gov	1992-10-04	081239004950
09054979656025876738	Lorenzo Lemerie	llemerie8@newyorker.com	\N	\N
79637781754587917528	Elyn Vaughan	evaughan9@mapquest.com	1988-12-09	082751877678
91431208368334322216	Madeline Yoxen	myoxena@shinystat.com	1990-04-06	085767262650
52148364448001622107	Cristi Schubuser	cschubuserb@ameblo.jp	1986-11-20	084621420476
37035872630850054065	Candy Dightham	cdighthamc@typepad.com	1985-09-25	086833305403
21594813884892270596	Waring Stutt	wstuttd@simplemachines.org	1985-06-16	082221576714
73672218364347144165	Miof mela Swatradge	mmelae@yellowpages.com	\N	081901052810
87147487624650243570	Amanda Minci	amincif@ox.ac.uk	1985-01-04	085217344330
73605563756248532873	Jeniece Dalglish	jdalglishg@google.ru	1981-02-27	086764881852
73506622399156528946	Camilla Spirritt	cspirritth@joomla.org	1990-05-13	086621210300
89031875319756622272	Almira Romagosa	aromagosai@phoca.cz	\N	087618037274
40243728783507793290	Doll Grayshon	dgrayshonj@ucsd.edu	1992-08-21	081154787515
08325231543252789834	Daisi Murrhaupt	dmurrhauptk@va.gov	1989-10-04	083176239758
98805058936524648004	Ruthe Igoe	rigoel@youku.com	1992-02-06	080430940686
64240430504127626817	Che Axton	caxtonm@shop-pro.jp	1988-08-17	089361876019
93995649247613633399	Raymond Bownas	rbownasn@123-reg.co.uk	1990-08-11	081601900600
58123227991230660569	Maribeth MacCaughan	mmaccaughano@irs.gov	1994-05-28	088016754932
72412300250595634568	Free Spurdle	fspurdlep@goo.ne.jp	\N	083359266251
15132931449404851787	Grady Annett	gannettq@live.com	1983-01-05	087596069005
67409469193068326386	Wallas Watford	wwatfordr@soup.io	\N	085182785436
60820969271826157051	Jeffry Moger	jmogers@last.fm	1990-07-24	082455717255
36840789280324764449	Aron Howison	ahowisont@sciencedaily.com	1995-02-17	\N
09326281851165173221	Hagen Quene	hqueneu@sourceforge.net	1991-09-05	089847535641
73249788789082064681	Lesly Broomfield	lbroomfieldv@cpanel.net	1983-01-17	085475792138
61635483574180442192	Corey Sonley	csonleyw@ucsd.edu	\N	\N
39911274904286030651	Reine Gosling	rgoslingx@geocities.jp	1987-10-30	084912658085
35402646113236398928	Vick Fader	vfadery@hhs.gov	1983-05-31	083010207739
42140288875353525504	Patric Duran	pduranz@engadget.com	1991-09-22	081733733719
95849046839115573948	Libbey Esberger	lesberger10@dagondesign.com	1983-09-25	089716304406
09643404999667107947	Loree Search	lsearch11@desdev.cn	1988-11-01	083693440678
28946547154693336808	Malanie Babbage	mbabbage12@deviantart.com	\N	\N
19840600485216889454	Bambie Megahey	bmegahey13@accuweather.com	1987-11-18	083815626604
42110701225884818282	Hall Hambers	hhambers14@about.me	1981-01-07	080072407670
42433278063120933230	Sven Diment	sdiment15@princeton.edu	1992-08-02	\N
98652913552510199993	Wolf McLean	wmclean16@dailymotion.com	1994-09-15	086333787971
85938622154195306824	Randal Lunn	rlunn17@is.gd	1990-02-04	086087028997
45166979530846509458	Chevalier Kohrs	ckohrs18@webmd.com	1993-10-06	081286646518
68528283622701707948	Ninette Magner	nmagner19@youtube.com	1989-11-15	082950015804
97009377697291677089	Douglass McCurtain	dmccurtain1a@free.fr	1982-10-09	083066906775
63562589239122852947	Arman Burker	aburker1b@narod.ru	\N	088643835557
78014214586158244148	Ramsay Trower	rtrower1c@gizmodo.com	1981-05-07	085898809159
42931284178733134589	Emlynn Swinfen	eswinfen1d@yellowbook.com	\N	084897733699
69807569402409459211	Belinda Nezey	bnezey1e@virginia.edu	1988-02-23	082824793000
10160532657929709480	Aggi Guillond	aguillond1f@oaic.gov.au	1990-04-22	087025386602
60171449186896735313	Dode Gavey	dgavey1g@ow.ly	1985-10-08	082464752971
52752754268141846440	Marjorie Girardy	mgirardy1h@ebay.co.uk	1986-02-08	\N
20948543328395218639	Tammie Brittle	tbrittle1i@rediff.com	1993-12-21	080590843732
05231756515975385982	Ellery Peake	epeake1j@google.co.uk	1982-07-21	088109136302
73315370623766663997	Seumas Freckelton	sfreckelton1k@state.gov	1992-06-12	\N
66025996395372277203	Titus Lenin	tlenin1l@opera.com	\N	087159953768
93741652064609229097	Howard Peeters	hpeeters1m@cocolog-nifty.com	1986-08-06	085227965491
69498951289378631191	Danny Elvin	delvin1n@engadget.com	1984-09-25	080343257844
72905554931015048977	Dennis Mackriell	dmackriell1o@histats.com	1980-12-01	080010343814
19948537697376309029	Beilul Brimilcome	bbrimilcome1p@adobe.com	1986-09-18	087180439198
24032874520349018291	Bayard Troppmann	btroppmann1q@livejournal.com	1985-12-25	\N
92765419216039307203	Archambault Pincott	apincott1r@admin.ch	1991-07-18	082772924687
03776350565147233050	Rafi Martinelli	rmartinelli1s@paypal.com	1984-11-13	088241243505
85871230537203328052	Tisha Hubbold	thubbold1t@merriam-webster.com	\N	\N
50035056544919604759	Lon Rangle	lrangle1u@barnesandnoble.com	1989-03-27	084826106647
50929912452266775152	Aubrie Bernet	abernet1v@google.cn	1982-08-26	088038758332
65239212634009723486	Birgitta Innocenti	binnocenti1w@patch.com	\N	086069682490
83170353179132776193	Devina Cressingham	dcressingham1x@spotify.com	\N	088998602854
72472468568913161583	Daile Nary	dnary1y@technorati.com	1989-04-28	081366617468
95155846837355220109	Odey Stubbley	ostubbley1z@a8.net	1991-01-02	084080052509
46835775042474062710	Georgena Boord	gboord20@paginegialle.it	1986-11-06	082063805949
38004888765418454368	Currie Maplestone	cmaplestone21@i2i.jp	1992-04-21	080690205232
86203763546087351236	Ahmed Tyres	atyres22@trellian.com	\N	083213175265
21255518690756411396	Samara Burdas	sburdas23@ft.com	1985-10-11	081571917614
28446781511455104547	Alard Adess	aadess24@nhs.uk	1984-05-14	083417238478
98173357558824190885	Vance Tarbath	vtarbath25@upenn.edu	\N	086919223400
07639382540165082999	Alick Le Leu	ale26@scribd.com	1983-09-11	084009304882
16312505491989204658	Micheil Malin	mmalin27@squidoo.com	1988-06-24	081582519854
85581315551970404633	Aldo Jamieson	ajamieson28@fotki.com	1989-08-19	\N
14165759167360865502	Shauna Tidbald	stidbald29@miitbeian.gov.cn	1980-05-26	085118041277
43839378479279291640	Suzanne Kirkham	skirkham2a@ocn.ne.jp	1992-10-22	086827980960
63510126440951680126	Yolane Twidale	ytwidale2b@mashable.com	1991-01-22	084553128017
66315564854348311821	Thadeus Ord	tord2c@ucsd.edu	\N	081414504029
40943312463493083803	Kendra Handscomb	khandscomb2d@google.it	1987-10-14	\N
73734061093203616225	Carny Digweed	cdigweed2e@hp.com	1982-10-16	086611989028
99209299096497566156	Melantha Tilston	mtilston2f@rambler.ru	1980-06-23	080014420345
72020110548056782343	Vernor Gouldie	vgouldie2g@mlb.com	\N	085882902023
14819673651963931895	Elston Crielly	ecrielly2h@dailymotion.com	1984-09-24	087331541014
89740790380502631922	Alysia Crisford	acrisford2i@edublogs.org	1980-05-20	\N
23240755615582069981	Rosemary Narrie	rnarrie2j@yahoo.co.jp	1981-01-07	082171130685
97174072491836012462	Drud Hanselmann	dhanselmann2k@amazon.de	1990-08-27	087462379170
03804228984688684410	Sandi Vina	svina2l@mac.com	1981-03-23	085112259654
56638514343485692856	Vail Pichmann	vpichmann2m@ameblo.jp	1994-06-06	\N
74024871534734781148	Larisa Titheridge	ltitheridge2n@photobucket.com	1987-04-13	081819662694
88500163593013419402	Audrie Count	acount2o@wix.com	1984-11-05	085163560754
99807022136199706322	Kerry Stubbin	kstubbin2p@theguardian.com	1986-04-03	081527315813
33865363860970895904	Ulrica Common	ucommon2q@washingtonpost.com	1985-09-19	085509669370
68649091168848100982	Nana Speirs	nspeirs2r@google.co.jp	\N	\N
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
1	1	Jemput	14518.4297	2019-01-01	98805058936524648004	Rumah
2	4	antar	13114.9902	2019-01-04	15132931449404851787	Rumah
3	5	antar	39748.8594	2019-01-05	66315564854348311821	Rumah
4	6	antar	40172.1602	2019-01-06	67409469193068326386	Kantor
5	7	antar	24044.4492	2019-01-07	63510126440951680126	Kantor
6	8	antar	10647.2695	2019-01-08	34021506316616141151	Kantor
7	10	Jemput	38336.3008	2019-01-10	09643404999667107947	Kantor
8	12	antar	28464.9707	2019-01-12	20948543328395218639	Rumah
9	14	antar	30388.1406	2019-01-14	65239212634009723486	Kantor
10	15	Jemput	4822.20996	2019-01-15	34021506316616141151	Kantor
11	17	Jemput	21241.1797	2019-01-17	68528283622701707948	Kantor
12	18	Jemput	33977.3594	2019-01-18	19948537697376309029	Rumah
13	19	antar	13814.3203	2019-01-19	40243728783507793290	Rumah
14	22	antar	33124.5508	2019-01-22	39911274904286030651	Kantor
15	24	antar	15605.4697	2019-01-24	98805058936524648004	Rumah
16	26	Jemput	37091.3789	2019-01-26	73605563756248532873	Kantor
17	27	antar	38313.9414	2019-01-27	37035872630850054065	Kantor
18	28	antar	4022.78003	2019-01-28	98805058936524648004	Rumah
19	29	Jemput	10683.3398	2019-01-29	36840789280324764449	Kantor
20	32	antar	23857.3594	2019-02-01	10160532657929709480	Kantor
21	33	antar	36198.2188	2019-02-02	40243728783507793290	Rumah
22	34	antar	36641.9297	2019-02-03	08325231543252789834	Kantor
23	35	antar	44903.2891	2019-02-04	79637781754587917528	Rumah
24	36	Jemput	17461.6895	2019-02-05	73672218364347144165	Kantor
25	38	Jemput	8993.46973	2019-02-07	78014214586158244148	Rumah
26	39	Jemput	34469.9219	2019-02-08	73734061093203616225	Rumah
27	42	Jemput	46295.0703	2019-02-11	58123227991230660569	Rumah
28	43	Jemput	36504.4297	2019-02-12	35402646113236398928	Rumah
29	44	antar	35610.2891	2019-02-13	86203763546087351236	Rumah
30	46	Jemput	19873.5391	2019-02-15	09643404999667107947	Kantor
31	49	antar	25648.4902	2019-02-18	20948543328395218639	Rumah
32	50	antar	18679.6504	2019-02-19	73506622399156528946	Rumah
33	5	antar	10000	2019-01-12	98805058936524648004	Rumah
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: toys_rent; Owner: db2018026
--

COPY toys_rent.status (nama, deskripsi) FROM stdin;
Sedang Dikonfirmasi	Pesanan sedang di cek untuk menunggu konfirmasi
Menunggu Pembayaran	Pesanan menunggu pembayaran anggota
Sedang Disiapkan	Pesanan sedang disiapkan
Sedang Dikirim	Pesanan sedang dikirim oleh kurir
Dalam Masa Sewa	Pesanan sedang disewa oleh anggota
Sudah dikembalikan	Pesanan sudah dikembalikan oleh anggota
Batal	Pesanan dibatalkan oleh anggota
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: barang_pesanan trigger_harga_sewa; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER trigger_harga_sewa AFTER INSERT ON toys_rent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.hitung_harga_sewa();


--
-- Name: pengiriman trigger_ongkos; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER trigger_ongkos AFTER INSERT OR UPDATE ON toys_rent.pengiriman FOR EACH ROW EXECUTE PROCEDURE toys_rent.hitung_ongkos();


--
-- Name: barang trigger_update_point_insert_barang; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER trigger_update_point_insert_barang AFTER INSERT ON toys_rent.barang FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_point_insert_barang();


--
-- Name: pemesanan trigger_update_point_pemesanan; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER trigger_update_point_pemesanan AFTER INSERT OR DELETE OR UPDATE ON toys_rent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_point_pemesanan();


--
-- Name: barang_pesanan update_insert_kondisi_barang_pesanan; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER update_insert_kondisi_barang_pesanan BEFORE INSERT ON toys_rent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_kondisi_barang_pesanan();


--
-- Name: anggota update_level_anggota; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER update_level_anggota AFTER INSERT OR UPDATE ON toys_rent.anggota FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_level_anggota();


--
-- Name: level_keanggotaan update_level_anggota_level; Type: TRIGGER; Schema: toys_rent; Owner: db2018026
--

CREATE TRIGGER update_level_anggota_level AFTER INSERT OR UPDATE ON toys_rent.level_keanggotaan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_level_anggota();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES toys_rent.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengembalian(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_nama_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_nama_status_fkey FOREIGN KEY (nama_status) REFERENCES toys_rent.status(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES toys_rent.admin(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES toys_rent.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_item kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_item kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES toys_rent.kategori(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES toys_rent.kategori(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama);


--
-- Name: pengembalian pengembalian_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018026
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

